//
//  RoomViewController.swift
//  OpenVoiceCall
//
//  Created by GongYuhua on 16/8/22.
//  Copyright © 2016年 Agora. All rights reserved.
//

import UIKit
import AgoraRtcKit
import MBProgressHUD
import Alamofire
import AVFoundation

protocol RoomVCDelegate: class {
    func roomVCNeedClose(_ roomVC: RoomViewController)
}

class RoomViewController: UIViewController {
    
    let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
    
    var saveUID:String!
    
    var getAllFriendsList:NSMutableArray! = []
    var strStatus = String()
    
    var getAllFriendsName:NSMutableArray! = []
    var getAllFriendsId:NSMutableArray! = []
    var getAllFriendsDeviceToken:NSMutableArray! = []
    
    // MARK:- SELECT GENDER -
    let regularFont = UIFont.systemFont(ofSize: 16)
    let boldFont = UIFont.boldSystemFont(ofSize: 16)
    
    @IBOutlet weak var roomNameLabel: UILabel!
    /*@IBOutlet weak var logTableView: UITableView! {
        didSet {
            // logTableView.dataSource = self
            logTableView.tableFooterView = UIView.init(frame: .zero)
        }
    }*/
    
    
    @IBOutlet weak var fullIncomingCallView:UIView! {
        didSet {
            fullIncomingCallView.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var btnIncomingCallDecline:UIButton! {
        didSet {
            btnIncomingCallDecline.backgroundColor = .systemRed
            btnIncomingCallDecline.setTitle("Decline", for: .normal)
            btnIncomingCallDecline.setTitleColor(.white, for: .normal)
            btnIncomingCallDecline.clipsToBounds = true
            btnIncomingCallDecline.layer.shadowColor = UIColor.black.cgColor
            btnIncomingCallDecline.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            btnIncomingCallDecline.layer.shadowOpacity = 1.0
            btnIncomingCallDecline.layer.shadowRadius = 2.0
            btnIncomingCallDecline.layer.masksToBounds = false
            btnIncomingCallDecline.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var btnIncomingCallAccept:UIButton! {
        didSet {
            btnIncomingCallAccept.backgroundColor = .systemGreen
            btnIncomingCallAccept.setTitle("Accept", for: .normal)
            btnIncomingCallAccept.setTitleColor(.white, for: .normal)
            btnIncomingCallAccept.clipsToBounds = true
            btnIncomingCallAccept.layer.shadowColor = UIColor.black.cgColor
            btnIncomingCallAccept.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            btnIncomingCallAccept.layer.shadowOpacity = 1.0
            btnIncomingCallAccept.layer.shadowRadius = 2.0
            btnIncomingCallAccept.layer.masksToBounds = false
            btnIncomingCallAccept.layer.cornerRadius = 12
        }
    }
    
    @IBOutlet weak var imgProfilePicture:UIImageView! {
        didSet {
            imgProfilePicture.layer.borderColor = UIColor.lightGray.cgColor
            imgProfilePicture.layer.borderWidth = 1
            imgProfilePicture.layer.cornerRadius = 70
            imgProfilePicture.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblCallerName:UILabel! {
        didSet {
            lblCallerName.textColor = .black
        }
    }
    
    @IBOutlet weak var muteAudioButton: UIButton!
    @IBOutlet weak var speakerButton: UIButton!
    
    var roomName: String!
    weak var delegate: RoomVCDelegate?
    
    
    @IBOutlet weak var btnAddFriend:UIButton! {
        didSet {
            btnAddFriend.tintColor = .black
        }
    }
    
    // create a reference for the Agora RTC engine
    fileprivate var agoraKit: AgoraRtcEngineKit!
    fileprivate var logs = [String]()
    
    // create a property for the Audio Muted state
    fileprivate var audioMuted = false {
        didSet {
            // update the audio button graphic whenever the audioMuted (bool) changes
            muteAudioButton?.setImage(UIImage(named: audioMuted ? "btn_mute_blue" : "btn_mute"), for: .normal)
            // use the audioMuted (bool) to mute/unmute the local audio stream
            agoraKit.muteLocalAudioStream(audioMuted)
        }
    }
    // create a property for the Speaker Mode state
    fileprivate var speakerEnabled = true {
        didSet {
            // update the speaker button graphics whenever the speakerEnabled (bool) changes
            speakerButton?.setImage(UIImage(named: speakerEnabled ? "btn_speaker_blue" : "btn_speaker"), for: .normal)
            speakerButton?.setImage(UIImage(named: speakerEnabled ? "btn_speaker" : "btn_speaker_blue"), for: .highlighted)
            // use the speakerEnabled (bool) to enable/disable speakerPhone
            agoraKit.setEnableSpeakerphone(speakerEnabled)
        }
    }
    
    @IBOutlet weak var lblWhoJoined:UILabel!
    
    // modified
    var loginUserNameIs:String!
    var arrSaveMultipleJoinedUsers:NSMutableArray = []
    
    var currentPageNumber:Int = 1
    var totalPage: Int = 0
    var arrJSON  = [AnyObject]()
    
    var fetchNewArray: [[String:Any]] = [[:]]
    
    @IBOutlet weak var btnENDCALL:UIButton! {
        didSet {
            btnENDCALL.backgroundColor = .systemRed
            btnENDCALL.layer.cornerRadius = 8
            btnENDCALL.clipsToBounds = true
            btnENDCALL.setTitle(" End Call", for: .normal)
            btnENDCALL.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var lblMuteOrUmmute:UILabel!
    
    
    var setSteps:String!
    var callerName:String!
    var callerImage:String!
    var dictOfNotificationPopup:NSDictionary!
    
    
    var strIamCallingTo:String!
    
    var totalHour = Int()
    var totalMinut = Int()
    var totalSecond = Int()
    var timer:Timer?
    
    var audioPlayer = AVAudioPlayer()
    
    @IBOutlet weak var lblCounter:UILabel! {
        didSet {
            lblCounter.textColor = .black
            lblCounter.isHidden = true
        }
    }
    
    
    @IBOutlet weak var receiverImageIs:UIImageView! {
        didSet {
            receiverImageIs.layer.borderColor = UIColor.lightGray.cgColor
            receiverImageIs.layer.borderWidth = 1
            receiverImageIs.layer.cornerRadius = 70
            receiverImageIs.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.loginUserNameIs = (dict["fullName"] as! String)
        
        // self.logTableView.separatorColor = .clear
        
        roomNameLabel.text = "\(roomName!)"
        print("\(roomName!)")
        
        self.btnAddFriend.addTarget(self, action: #selector(callListOfAllFriends), for: .touchUpInside)
        
        
        let clickSound = URL(fileURLWithPath: Bundle.main.path(forResource: "inOrOut", ofType: "mp3")!)
        
        
        
        // self.fullIncomingCallView.isHidden = true
        if "\(setSteps!)" == "1" { // call is incoming
            
            agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId: KeyCenter.AppId, delegate: self)
            agoraKit.delegate = self
            
            print("\(setSteps!)")
            print("\(callerName!)")
            print("Image========>\(callerImage!)")
            
            self.lblCallerName.text = "\(callerName!)"
            self.fullIncomingCallView.isHidden = false
            self.imgProfilePicture.dowloadFromServer(link:"\(callerImage!)", contentMode: .scaleToFill)
            
            self.receiverImageIs.isHidden = true
            self.receiverImageIs.dowloadFromServer(link:"\(callerImage!)", contentMode: .scaleToFill)
            
            do {
                
                 audioPlayer = try AVAudioPlayer(contentsOf: clickSound)
                 audioPlayer.play()
                
            } catch {
                
            }
            
        } else if "\(setSteps!)" == "2" { // who start the call
            
            self.roomNameLabel.text = "\(callerName!)"
            
            self.fullIncomingCallView.isHidden = true
            self.receiverImageIs.isHidden = false
            self.receiverImageIs.dowloadFromServer(link:"\(callerImage!)", contentMode: .scaleToFill)
            self.loadAgoraKit()
            
            do {
                
                 audioPlayer = try AVAudioPlayer(contentsOf: clickSound)
                 audioPlayer.play()
                
            } catch {
                
            }
            
            // self.lblCallerName.text = "\(callerName!)"
            // self.imgProfilePicture.dowloadFromServer(link:"\(callerImage!)", contentMode: .scaleToFill)
            
        }
        
        self.btnIncomingCallAccept.addTarget(self, action: #selector(incomingAcceptClickMethod), for: .touchUpInside)
        self.btnIncomingCallDecline.addTarget(self, action: #selector(incomingDeclineClickMethod), for: .touchUpInside)
        
        // logTableView.rowHeight = UITableView.automaticDimension
        // logTableView.estimatedRowHeight = 50
        // loadAgoraKit()
    }
    
    @objc func incomingAcceptClickMethod() {
        audioPlayer.stop()
        if "\(setSteps!)" == "1" { // call is incoming and you decline
            
            self.fullIncomingCallView.isHidden = true
            self.receiverImageIs.isHidden = false
            self.loadAgoraKit()
            
            self.roomNameLabel.text = "\(callerName!)"
             
        } else {
            
        }
        
    }
    
    @objc func incomingDeclineClickMethod() {
        audioPlayer.stop()
        
        if "\(setSteps!)" == "1" { // call is incoming and you decline
            agoraKit.leaveChannel(nil)
            delegate?.roomVCNeedClose(self)
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "DashboardVC")
            self.navigationController?.pushViewController(push, animated: true)
            
        } else {
            
        }
        
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countdown), userInfo: nil, repeats: true)
    }

    @objc func countdown() {
        var hours: Int
        var minutes: Int
        var seconds: Int

        /*if totalSecond == 0 {
            timer?.invalidate()
        }*/
        totalSecond = totalSecond + 1
        hours = totalSecond / 3600
        minutes = (totalSecond % 3600) / 60
        seconds = (totalSecond % 3600) % 60
        // timeLabel.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        
        print(String(format: "%02d:%02d:%02d", hours, minutes, seconds))
        
        self.lblCounter.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    @IBAction func doMuteAudioPressed(_ sender: UIButton) {
        audioMuted = !audioMuted
    }
    
    @IBAction func doSpeakerPressed(_ sender: UIButton) {
        speakerEnabled = !speakerEnabled
    }
    
    @IBAction func doClosePressed(_ sender: UIButton) {
        leaveChannel()
    }
    
    
    
    @objc func callListOfAllFriends() {
        self.getAllFriendsList.removeAllObjects()
        
        let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
        let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
        spinnerActivity.label.text = "Loading";
        spinnerActivity.detailsLabel.text = "Please Wait!!";
        
        if let apiString = URL(string:BaseUrl) {
            var request = URLRequest(url:apiString)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            if (strStatus == "0") {
                let values = ["action":"friendlist","userId": String(dict["userId"]as? Int ?? 0),"status":"0","pageNo":"0"]as [String : Any]as [String : Any]
                print("Values \(values)")
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
            }
            else {
                let values = ["action":"friendlist","userId": String(dict["userId"]as? Int ?? 0),"status":"1","pageNo":"0"]as [String : Any]as [String : Any]
                print("Values \(values)")
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
            }
            
            AF.request(request)
                .responseJSON { response in
                    // do whatever you want here
                    MBProgressHUD.hide(for:self.view, animated: true)
                    switch response.result {
                    case .failure(let error):
                        let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            print("you have pressed the Ok button");
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion:nil)
                        print(error)
                        if let data = response.data,
                           let responseString = String(data: data, encoding: .utf8) {
                            print(responseString)
                            print("response \(responseString)")
                        }
                    case .success(let responseObject):
                        DispatchQueue.main.async {
                            print("This is run on the main queue, after the previous code in outer block")
                        }
                        print("response \(responseObject)")
                        
                        MBProgressHUD.hide(for:self.view, animated: true)
                        
                        /*
                         device = Android;
                         deviceToken = "cd0USqt5T1-NEgaBEtbO1G:APA91bHb_Vimw7U8b8TegGtMiMO_57M9W5p3Wkd8hiZ2KvTJ1GcrNEc5xsTDP6oQOzq-c0PXBtGsitCzrwqrSbCM56eSvggdLMnmPFkyJJG8uqgUKZVQVGsZ1ibNwFOX3h3S3_wV_s6k";
                         friendsId = 128;
                         profession = developer;
                         profileId = 149;
                         "profile_picture" = "";
                         status = 1;
                         userAddress = "ramprastha ";
                         userEmail = "mona11@gmail.com";
                         userId = 149;
                         userLatitude = "28.6634125";
                         userLongitude = "77.3239855";
                         userName = mona11;
                         userPhone = 9689563212;
                         */
                        
                        
                        
                        
                        
                        if let dict = responseObject as? [String:Any] {
                            
                            
                            
                            let status = dict["status"]as? String
                            if(status != "Fails") {
                                self.fetchNewArray.removeAll()
                                
                                if (self.currentPageNumber == 1) {
                                    
                                    self.arrJSON  = (dict["data"] as AnyObject) as! [AnyObject]
                                    
                                    
                                    var ar : NSArray!
                                    ar = (dict["data"] as! Array<Any>) as NSArray
                                    
                                    self.getAllFriendsList.addObjects(from: ar as! [Any])
                                    
                                    self.addMoreFriendOnChat()
                                    
                                    
                                    // self.getAllFriendsList2 .add(self.arrJSON)
                                } else {
                                    
                                    self.fetchNewArray  = ((dict["data"] as AnyObject) as! [AnyObject] as NSArray) as! [[String : Any]]
                                    
                                }
                                if self.arrJSON.count != 0 {
                                    
                                    if (self.currentPageNumber == 1) {
                                        // self.tbleView.isHidden = false
                                        // self.lblNoRecord.isHidden = true
                                        // self.tbleView.reloadData()
                                    } else {
                                        
                                        if (self.fetchNewArray.count != 0) {
                                            
                                            // self.didFinishRecordsRequest(result: (self.fetchNewArray) as NSArray, pageNo: self.currentPageNumber)
                                            
                                        } else {
                                            
                                            self.currentPageNumber -= 1
                                            
                                        }
                                    }
                                }else{
                                    // self.lblNoRecord.isHidden = false
                                    // self.tbleView.isHidden = true
                                }
                            }else{
                                // self.lblNoRecord.isHidden = false
                                // self.tbleView.isHidden = true
                            }
                        }
                        
                    }
                }
        }
    }
    
    @objc func addMoreFriendOnChat() {
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        /*print(self.getAllFriendsList as Any)
        /*
         device = Android;
         deviceToken = "cd0USqt5T1-NEgaBEtbO1G:APA91bHb_Vimw7U8b8TegGtMiMO_57M9W5p3Wkd8hiZ2KvTJ1GcrNEc5xsTDP6oQOzq-c0PXBtGsitCzrwqrSbCM56eSvggdLMnmPFkyJJG8uqgUKZVQVGsZ1ibNwFOX3h3S3_wV_s6k";
         friendsId = 128;
         profession = developer;
         profileId = 149;
         "profile_picture" = "";
         status = 1;
         userAddress = "ramprastha ";
         userEmail = "mona11@gmail.com";
         userId = 149;
         userLatitude = "28.6634125";
         userLongitude = "77.3239855";
         userName = mona11;
         userPhone = 9689563212;
         */
        
        
        for check in 0...self.getAllFriendsList.count-1 {
            print(check as Any)
            
            let item = self.getAllFriendsList[check] as? [String:Any]
            print(item as Any)
            
            let strName:String!
            let strId:String!
            let strToken:String!
            
            strName = (item!["userName"] as! String)
            
            
            strToken = (item!["deviceToken"] as! String)
            
            
            
            let x : Int = item!["userId"] as! Int
            let myString = String(x)
            strId = String(myString)
            
            
            self.getAllFriendsName.add(strName as Any)
            self.getAllFriendsId.add(strId as Any)
            self.getAllFriendsDeviceToken.add(strToken as Any)
            
        }
        
        // print(self.getAllFriendsDeviceToken as Any)
        
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "Add Friend",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : .systemBlue,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "Search friend",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : .systemBlue,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
        
        let array: [String] = self.getAllFriendsName.copy() as! [String]
        
        let arrGender = array
        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
                                       onCompletion: { [self] (selectedIndexes, selectedValues) in
                                        if let selectedValue = selectedValues.first{
                                            if selectedValue == arrGender.last! {
                                                
                                                // self.btnGenderPicker.setTitle("I'm a human", for: .normal)
                                                // cell.txtSelectGender.text = "I'm a human"
                                                
                                                let index = self.getAllFriendsName.index(of: "\(selectedValue)");
                                                let item2 = self.getAllFriendsList[index] as? [String:Any]
                                                
                                                print((item2!["deviceToken"] as! String) as Any)
                                                
                                                
                                                self.sendNotificationToOtherUser(strFriendToken: (item2!["deviceToken"] as! String), strFriendRoomName: "\(roomName!)")
                                                
                                            } else {
                                                
                                                // self.btnGenderPicker.setTitle("I'm \(selectedValue)", for: .normal)
                                                // cell.txtSelectGender.text = "\(selectedValue)"
                                                
                                                let index = self.getAllFriendsName.index(of: "\(selectedValue)");
                                                let item2 = self.getAllFriendsList[index] as? [String:Any]
                                                
                                                print((item2!["deviceToken"] as! String) as Any)
                                                
                                                self.sendNotificationToOtherUser(strFriendToken: (item2!["deviceToken"] as! String), strFriendRoomName: "\(roomName!)")
                                            }
                                        } else {
                                            
                                            // self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                                            // cell.txtSelectGender.text = "What's your gender?"
                                            
                                        }
                                       },
                                       onCancel: {
                                        print("Cancelled")
                                       }
        )
        
        picker.show(withAnimation: .FromBottom)*/
    }
    
    @objc func sendNotificationToOtherUser(strFriendToken:String,strFriendRoomName:String) {
        
        // self.callOurLocalServer(strRandomIdIs: String(dictGetPostData["postRandomGenerateNumber"] as! String), strTimeStamp: String(myTimeStamp))*/
        
        let token = String(strFriendToken)
        
        let serverKey = "AAAA2MXCPOc:APA91bGiS3JSksRtfihf_SP8zHr-4fst0lqqCCVCbH-IHOq-vZkZoiMp3FKLnSVD9eQj614WDqEWcIPcxmDGvg_lYqfqYfYEdxJSUankOeMQ54lg2x8EGWCLVpFNX660OInJz1pgTwp5"
        let partnerToken = token
        // let topic = "/topics/<Dishant Rajput>"  // replace it with partnerToken if you want to send a topic
        let url = NSURL(string: "https://fcm.googleapis.com/fcm/send")

        let postParams = [
            "to": partnerToken,
            "notification": [
                "body": "Incoming Audio Call",
                "title": (dict["fullName"] as! String),
                "sound" : true,
                // "click_action" : String(self.chatChannelName),
                // "click_action2" : String(self.chatChannelName),
            ],
            "data":[
                "roomName" : String(strFriendRoomName),
                "receiverImage":String("")
            ]] as [String : Any]

           let request = NSMutableURLRequest(url: url! as URL)
            request.httpMethod = "POST"
            request.setValue("key=\(serverKey)", forHTTPHeaderField: "Authorization")
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")

          do {
                request.httpBody = try JSONSerialization.data(withJSONObject: postParams, options: JSONSerialization.WritingOptions())
                print("My paramaters: \(postParams)")
            
            
            } catch {
                print("Caught an error: \(error)")
            }


        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            if let realResponse = response as? HTTPURLResponse {
                if realResponse.statusCode != 200 {
                    print("Not a 200 response")
                }
            }

            if let postString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as String? {
                print("POST: \(postString)")
            }
        }

             task.resume()
        
        
        
        
        
        
    }
}

private extension RoomViewController {
    func append(log string: String) {
        guard !string.isEmpty else {
            return
        }
        
        logs.append(string)
       
    }
    
    func updateLogTable(withDeleted deleted: String?) {
        
        /*guard let tableView = logTableView else {
            return
        }
        
        let insertIndexPath = IndexPath(row: logs.count - 1, section: 0)
        
        tableView.beginUpdates()
        if deleted != nil {
            tableView.deleteRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        }
        tableView.insertRows(at: [insertIndexPath], with: .none)
        tableView.endUpdates()
        
        tableView.scrollToRow(at: insertIndexPath, at: .bottom, animated: false)*/
    }
    
}

//MARK: - table view -
 extension RoomViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSaveMultipleJoinedUsers.count  // logs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "logCell", for: indexPath) as! LogCell
        
        let item = self.arrSaveMultipleJoinedUsers[indexPath.row] as! [String:Any]
        cell.logLabel.text = (item["name"] as! String)
         
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        print("clicked")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        let item = self.arrSaveMultipleJoinedUsers[indexPath.row] as! [String:Any]
        
         if (item["status"] as! String) == "yes" {
            return 60
        } else {
            return 0
        }
        
    }
    
    
 }

extension RoomViewController: UITableViewDelegate {
   
}

//MARK: - agora engine -
private extension RoomViewController {
    
    func loadAgoraKit() {
        
        agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId: KeyCenter.AppId, delegate: self)
        
        // Assign delegate later
        agoraKit.delegate = self
        
        // let mergeNameAndImage =
        
        let registerResponse: Int32 = agoraKit.registerLocalUserAccount(String(self.loginUserNameIs), appId: KeyCenter.AppId)
        
        if registerResponse == 0 {
            print("Successfully registered")
            
            agoraKit.joinChannel(byUserAccount: String(self.loginUserNameIs), token: nil, channelId: String(roomName)) { (channel, uid, elapsed) in
                print("User joined channel \(channel) with \(uid). Elapsed time is \(elapsed)ms.")
            }
            
            
        } else {
            
            print("Failed to register")
            
        }
        
        
    }
    
    func leaveChannel() {
        // leaving the Agora channel
        agoraKit.leaveChannel(nil)
        delegate?.roomVCNeedClose(self)
        
        self.timer?.invalidate()
        
        if "\(setSteps!)" == "1" {
            //
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "DashboardVC")
            self.navigationController?.pushViewController(push, animated: true)
            
        } else {
            
            let alertController = UIAlertController(title: "Call end", message: nil, preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
                self.navigationController?.popViewController(animated: true)
                
            }
             
            alertController.addAction(cancel)
            self.present(alertController, animated: true, completion:nil)
            
            
        }
        
        /*if "\(setSteps!)" == "1" {
            
             let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChatFriendsListingVC")
             self.navigationController?.pushViewController(push, animated: true)
            
        } else {
            self.navigationController?.popViewController(animated: true)
        }*/
        
        
        // let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChatListingVC")
        // self.navigationController?.pushViewController(push, animated: true)
        
    }
    
}

//MARK: Agora Delegate
extension RoomViewController: AgoraRtcEngineDelegate {
    
    func rtcEngineConnectionDidInterrupted(_ engine: AgoraRtcEngineKit) {
        append(log: "Connection Interrupted")
    }
   
    func rtcEngineConnectionDidLost(_ engine: AgoraRtcEngineKit) {
        append(log: "Connection Lost")
    }
   
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        append(log: "Occur error: \(errorCode.rawValue)")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didUpdatedUserInfo userInfo: AgoraUserInfo, withUid uid: UInt) {
        
        if userInfo.userAccount == nil {
            
        } else {
            
            print(userInfo.uid as Any)
            print(userInfo.userAccount as Any)
            
            let myDictionary: [String:String] = [
            
                "id"        : String(userInfo.uid),
                "name"      : String(userInfo.userAccount!),
                "status"    : "yes"
            ]
            
            /*var res = [[String: String]]()
            res.append(myDictionary)*/
            
            self.arrSaveMultipleJoinedUsers.add(myDictionary)
            
            // print(self.arrSaveMultipleJoinedUsers as Any)
            
            if self.arrSaveMultipleJoinedUsers.count >= 1 {
                self.roomNameLabel.text = "On Call with"
                self.lblCounter.isHidden = false
                // self.startTimer()
            } else {
                self.timer?.invalidate()
                
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChatListingVC")
                self.navigationController?.pushViewController(push, animated: true)
                
            }
            
            // self.logTableView.delegate = self
            // self.logTableView.dataSource = self
            // self.logTableView.reloadData()
            
        }
        
    }
   
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int) {
        append(log: "Did joined channel: \(channel), with uid: \(uid), elapsed: \(elapsed)")
        
        print("i am 8 plus")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        append(log: "Did joined of uid: \(uid)")
        
        
        audioPlayer.stop()
    }
   
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        append(log: "Did offline of uid: \(uid), reason: \(reason.rawValue)")
        
        
        for indexx in 0..<self.arrSaveMultipleJoinedUsers.count {
            
            let item = self.arrSaveMultipleJoinedUsers[indexx] as! [String:Any]
            
            if (item["id"] as! String) == "\(uid)" {
                
                print(indexx as Any)
                
                self.arrSaveMultipleJoinedUsers.removeObject(at: indexx)
                
                let myDictionary: [String:String] = [
                
                    "id"        : (item["id"] as! String),
                    "name"      : (item["name"] as! String),
                    "status"    : "no"
                ]
                
                
                /*var res = [[String: String]]()
                res.append(myDictionary)*/
                
                self.arrSaveMultipleJoinedUsers.insert(myDictionary, at: indexx)
                
                print(self.arrSaveMultipleJoinedUsers.count as Any)
                
                if self.arrSaveMultipleJoinedUsers.count == 0 {
                    
                    self.timer?.invalidate()
                    
                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChatListingVC")
                    self.navigationController?.pushViewController(push, animated: true)
                    
                } else {
                    
                    // self.roomNameLabel.text = "On Call with"
                    // self.lblCounter.isHidden = false
                    // self.startTimer()
                }
                
                
                // self.logTableView.reloadData()
                
                
            } else {
                
            }
            
        }
        
        print("TOTAL USER IN THIS CHAT IS ==== >",self.arrSaveMultipleJoinedUsers.count as Any)
        
        if self.arrSaveMultipleJoinedUsers.count == 0 {
            self.leaveChannel()
            
            
            
        } else if self.arrSaveMultipleJoinedUsers.count == 1 {
            self.leaveChannel()
            
            
        } else {
            
        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, audioQualityOfUid uid: UInt, quality: AgoraNetworkQuality, delay: UInt, lost: UInt) {
        append(log: "Audio Quality of uid: \(uid), quality: \(quality.rawValue), delay: \(delay), lost: \(lost)")
    }
  
    func rtcEngine(_ engine: AgoraRtcEngineKit, didApiCallExecute api: String, error: Int) {
        append(log: "Did api call execute: \(api), error: \(error)")
    }
}


