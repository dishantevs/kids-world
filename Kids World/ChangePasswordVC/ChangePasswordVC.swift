//
//  ChangePasswordVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import MBProgressHUD

class ChangePasswordVC: UIViewController {
    
    @IBOutlet weak var viewBG:UIView!
    @IBOutlet weak var txtOldPass: UITextField!
    @IBOutlet weak var txtNewPass:UITextField!
    @IBOutlet weak var txtConfirmPass:UITextField!
    
    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnBack: UIButton!

    var responseDictionary:[String:Any] = [:]
    var dataDictionary:[String:Any] = [:]
    var status: String?
    var msg: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roundedShadowBorderOnView(view: viewBG)
        btnBack.centerTextAndImage(spacing: 10.0)
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
    }
    
    
    // MARK :- button Action
    @IBAction func btnBackClickMe(_ sender : UIButton){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardVC") as? DashboardVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
    @IBAction func submitButtonTapped(_ sender: Any) {
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            if String(txtOldPass.text ?? "") == "" {
                self.validationAlertPopup(strAlertTitle: "Alert", strMessage: "Email Address ", strMessageAlert: "should not be Empty")
            } else if String(txtNewPass.text ?? "") == "" {
                self.validationAlertPopup(strAlertTitle: "Alert", strMessage: "Password ", strMessageAlert: "should not be Empty")
            } else if String(txtNewPass.text ?? "") != String(txtConfirmPass.text ?? "") {
                self.validationAlertPopup(strAlertTitle: "Alert", strMessage: "Please make sure ", strMessageAlert: "your passwords match.")
            } else{
                let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
                spinnerActivity.label.text = "Loading";
                spinnerActivity.detailsLabel.text = "Please Wait!!";
                
                if let apiString = URL(string:BaseUrl) {
                    var request = URLRequest(url:apiString)
                    request.httpMethod = "POST"
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    
                    let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
                    let values = ["action":"changePassword",
                                  "oldPassword":txtOldPass.text ?? "",
                                  "newPassword":txtNewPass.text ?? "",
                                  "userId": String(dict["userId"]as? Int ?? 0)]as [String : Any]as [String : Any]
                    
                    print("Values \(values)")
                    request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                    AF.request(request)
                        .responseJSON { response in
                            MBProgressHUD.hide(for:self.view, animated: true)
                            switch response.result {
                            case .failure(let error):
                                let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    print("you have pressed the Ok button");
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion:nil)
                                print(error)
                                if let data = response.data,
                                   let responseString = String(data: data, encoding: .utf8) {
                                    print(responseString)
                                    print("response \(responseString)")
                                }
                            case .success(let responseObject):
                                print("response \(responseObject)")
                                MBProgressHUD.hide(for:self.view, animated: true)
                                if let dict = responseObject as? [String:Any] {
                                    self.responseDictionary = dict
                                    self.status = self.responseDictionary["status"] as? String
                                    let alertController = UIAlertController(title: "Alert", message: self.responseDictionary["msg"]as? String ?? "", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                        if self.status != "Fails"{
                                            self.txtNewPass.text = ""
                                            self.txtOldPass.text = ""
                                            self.txtConfirmPass.text = ""
                                            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardVC") as? DashboardVC
                                            self.navigationController?.pushViewController(vc!, animated: false)
                                        }
                                    }
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                        }
                }
            }
        }
    }
}
