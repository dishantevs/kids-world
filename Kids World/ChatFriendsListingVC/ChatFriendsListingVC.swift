//
//  VideosListingVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

class ChatFriendsListingVC: UIViewController,UITableViewDataSource, UITableViewDelegate ,UITextFieldDelegate {
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch : UITextField!
    @IBOutlet weak var lblNoRecord: UILabel!

    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    
    var responseDictionary:[String:Any] = [:]
    var responseArray:[[String:Any]] = [[:]]
    @IBOutlet weak var tbleView: UITableView!
    
    //sam
    var currentPageNumber:Int = 1
    var totalPage: Int = 0
    var arrJSON  = [AnyObject]()
    var fetchNewArray: [[String:Any]] = [[:]]

    var searching:Bool = false
    var arrSearchJSON  = [AnyObject]()
    // slider//
    var isOpen = false
    var screenCheck = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewSearch.layer.cornerRadius = 10
        txtSearch.delegate = self
        btnMenu.centerTextAndImage(spacing: 10.0)
        btnCart.centerTextAndImage(spacing: 10.0)
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)

        // slider //
        self.btnMenu.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        let directions: [UISwipeGestureRecognizer.Direction] = [.left,.right,.up,.down]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(DashboardVC.handleSwipe1(gesture:)))
            gesture.direction = direction
            self.view?.addGestureRecognizer(gesture)
        }
        
        self.arrJSON.removeAll()
        self.tbleView.isHidden = true
        self.getChatFriendsListingAPI(pageNo : currentPageNumber, str_search_key: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    var receiverDic : [String :Any] = [:]
    var arrSearchNewArray: [[String:Any]] = [[:]]
    
    @IBAction func btnSearchClickMe(sender : UIButton) { //     userName = newnew;
        /*self.arrSearchJSON.removeAll()
        searching = true

        for index in 0..<self.arrJSON.count {
            let element = self.arrJSON[index]
            if element["userName"]as?String ?? "" == txtSearch.text ?? "" {
                self.arrSearchJSON.append(element)
            }
        }
        if self.arrSearchJSON.count != 0{
            self.tbleView.reloadData()
        }else{
            let alert = UIAlertController(title: "Alert", message: "No Record Found", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }*/
        
        self.getChatFriendsListingAPI(pageNo: 1,str_search_key:String(txtSearch.text!))
        
    }

    
    func getChatFriendsListingAPI(pageNo: Int,str_search_key:String) {
        
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
                
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                let values = ["action":"friendlist",
                              "userId": String(dict["userId"]as? Int ?? 0),
                              "status":"1",
                              "keyword":String(str_search_key)] as [String : Any]
                
                print("Values \(values)")
                
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                
                AF.request(request)
                    .responseJSON { response in
                        // do whatever you want here
                        MBProgressHUD.hide(for:self.view, animated: true)
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            // self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            
                            if let dict = responseObject as? [String:Any] {
                                let status = dict["status"]as? String
                                if(status != "Fails"){
                                    self.fetchNewArray.removeAll()
                                    
                                    self.arrJSON  = (dict["data"] as AnyObject) as! [AnyObject]
                                    
                                    /*if (self.currentPageNumber == 1) {
                                        
                                        self.arrJSON  = (dict["data"] as AnyObject) as! [AnyObject]
                                        
                                    } else {
                                        
                                        self.fetchNewArray  = ((dict["data"] as AnyObject) as! [AnyObject] as NSArray) as! [[String : Any]]
                                        
                                    }*/
                                    
                                    self.tbleView.isHidden = false
                                    self.lblNoRecord.isHidden = true
                                    self.tbleView.reloadData()
                                    
                                    /*if self.arrJSON.count != 0 {
                                        
                                        if (self.currentPageNumber == 1) {
                                            
                                            self.tbleView.isHidden = false
                                            self.lblNoRecord.isHidden = true
                                            self.tbleView.reloadData()
                                            
                                        } else {
                                            
                                            if (self.fetchNewArray.count != 0) {
                                                self.didFinishRecordsRequest(result: (self.fetchNewArray) as NSArray, pageNo: self.currentPageNumber)
                                            }else {
                                                self.currentPageNumber -= 1
                                            }
                                        }
                                        
                                    } else {
                                        self.lblNoRecord.isHidden = false
                                        self.tbleView.isHidden = true
                                    }*/
                                } else {
                                    self.lblNoRecord.isHidden = false
                                    self.tbleView.isHidden = true
                                }
                            }
                            
                        }
                    }
            }
        }
        }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if searching == true{
        }else{
            let lastItem = self.arrJSON.count - 1
            if indexPath.row == lastItem {
                print("IndexRow :: \(indexPath.row)")
                self.currentPageNumber += 1
            }
        }
     }

     func didFinishRecordsRequest(result:NSArray, pageNo:Int) {
         if (pageNo != 0){
             self.arrJSON += result as Array<AnyObject>
         } else {
             self.arrJSON.removeAll()
             self.arrJSON = result.mutableCopy() as! [AnyObject]
         }
         self.totalPage = Int(self.arrJSON.count)
         self.tbleView.reloadData()
     }

    
    //MARK: Table and Collection View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*if searching == true{
            return self.arrSearchJSON.count
        }else if searching == false{
            return self.arrJSON.count
        }*/
        return self.arrJSON.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SendFriendListTableCell = tableView.dequeueReusableCell(withIdentifier: "SendFriendListTableCell") as! SendFriendListTableCell
        
        var dict : [String :Any] = [:]
        /*if searching == true{
            dict = self.arrSearchJSON[indexPath.row] as! [String : Any]
            print(dict as Any)
        }else if searching == false{
            dict = self.arrJSON[indexPath.row] as! [String : Any]
            print(dict as Any)
        }*/
           
        dict = self.arrJSON[indexPath.row] as! [String : Any]
        
        cell.lblTitle.text = (dict["userName"]as?String ?? "").capitalized
        cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.height / 2
        cell.imgProfile.clipsToBounds = true
        cell.imgProfile.dowloadFromServer(link:dict["profile_picture"]as?String ?? "", contentMode: .scaleToFill)
        
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var dict : [String :Any] = [:]
        if searching == true{
            dict = self.arrSearchJSON[indexPath.row] as! [String : Any]
            print(dict as Any)
        }else if searching == false{
            dict = self.arrJSON[indexPath.row] as! [String : Any]
            print(dict as Any)
        }
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BooCheckChat") as? BooCheckChat
        let x : Int = (dict["profileId"] as? Int ?? 0)
        let myString = String(x)
        push!.strReceiptId = String(myString)
        push!.strReceiptImage = (dict["profile_picture"] as! String)
        push!.receiverData = dict as NSDictionary?
        push!.friendDeviceToken = (dict["deviceToken"] as! String)
        
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func connected(sender: UIButton) {
        
        var dict : [String :Any] = [:]
        
        if searching == true {
            
            dict = arrSearchJSON[sender.tag] as! [String : Any]
            print(dict as Any)
            
        } else if searching == false {
            
            dict = arrJSON[sender.tag] as! [String : Any]
            print(dict as Any)
            
        }
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert!", message: "Choose an option!", preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        
        actionSheetController.addAction(cancelAction)
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Post Report", style: .default) { action -> Void in
            let uiAlert = UIAlertController(title: "Report", message: "\nAre you sure want to report this user?", preferredStyle: UIAlertController.Style.alert)
            self.present(uiAlert, animated: true, completion: nil)
            
            uiAlert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
            }))
            uiAlert.addAction(UIAlertAction(title: "Yes, Report", style: .default, handler: { action in
                 self.callCommonAPI(strAction: "report",  profileId: dict["profileId"]as?Int ?? 0)
            }))
        }
        actionSheetController.addAction(takePictureAction)
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Block this User", style: .default) { action -> Void in
            let uiAlert = UIAlertController(title: "Block", message: "\nAre you sure want to block this user?\n\n This user will permanently deleted from your list.", preferredStyle: UIAlertController.Style.alert)
            self.present(uiAlert, animated: true, completion: nil)
            
            uiAlert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
            }))
            uiAlert.addAction(UIAlertAction(title: "Yes, Block", style: .default, handler: { action in
                self.callCommonAPI(strAction: "blockuser", profileId: dict["profileId"]as?Int ?? 0)
            }))
        }
        actionSheetController.addAction(choosePictureAction)
        self.present(actionSheetController, animated: true, completion: nil)
        
    }

    func callCommonAPI(strAction: String , profileId : Int) {
        
        if isInternetAvailable() == false {
            
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        else {
            
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                if(strAction == "blockuser"){ // blocker_id == user id
                    let values = ["action":strAction, "blockee_id":profileId, "blocker_id":String(dict["userId"]as? Int ?? 0)]as [String : Any]as [String : Any]
                    print("Values \(values)")
                    request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                }else{
                    let values = ["action":strAction, "commentId":profileId]as [String : Any]as [String : Any]
                    print("Values \(values)")
                    request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                }
                
                AF.request(request)
                    .responseJSON { response in
                        MBProgressHUD.hide(for:self.view, animated: true)
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                let alertController = UIAlertController(title: "Alert", message:dict["msg"] as? String ?? "" , preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    if(dict["status"] as? String ?? "" == "success"){
                                        if(strAction == "blockuser") {
                                            self.navigationController?.popViewController(animated: false)
                                        }
                                    }
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                    }
                }
            
            }
        
        }
    
    
    // Left Slider //
    @objc func handleSwipe1(gesture: UISwipeGestureRecognizer) {
        print(gesture.direction)
        switch gesture.direction {
        
        case UISwipeGestureRecognizer.Direction.left:
            isOpen = false
            if screenCheck == true{
                let viewMenuBack : UIView = view.subviews.last!
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    var frameMenu : CGRect = viewMenuBack.frame
                    frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                    viewMenuBack.frame = frameMenu
                    viewMenuBack.layoutIfNeeded()
                    viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
                })
                screenCheck = false
            }else{
            }
            print("left swipe")
        case UISwipeGestureRecognizer.Direction.right:
            print("right swipe")
            isOpen = true
            if screenCheck == false{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let menuVC  = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController
                self.view.addSubview(menuVC!.view)
                self.addChild(menuVC!)
                menuVC!.view.layoutIfNeeded()
                
                menuVC!.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 85, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 85);
                
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    menuVC!.view.frame=CGRect(x: 0, y: 85, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 85);
                }, completion:nil)
                screenCheck = true
            }else{
            }
        default:
            print("other swipe")
        }
    }
    @objc func sideBarMenuClick() {
        if(!isOpen){
            screenCheck = true
            isOpen = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController
            self.view.addSubview(menuVC!.view)
            self.addChild(menuVC!)
            menuVC!.view.layoutIfNeeded()
            
            menuVC!.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 85, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 85);
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC!.view.frame=CGRect(x: 0, y: 85, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 85);
            }, completion:nil)
            
        }else if(isOpen){
            screenCheck = false
            isOpen = false
            let viewMenuBack : UIView = view.subviews.last!
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.count <= 0 {
            searching = false
            tbleView.reloadData()
        }
        return true
    }
}
