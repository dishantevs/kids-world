//
//  CommentsVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//
 
import UIKit
import GrowingTextView
import Firebase
import FirebaseStorage
import SDWebImage
import Alamofire
import SwiftyJSON
import MBProgressHUD

class CommentsVC: UIViewController, MessagingDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var textView: GrowingTextView!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSendMessage:UIButton!

    var DictGetValue : [String :Any] = [:]
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblLikes: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!

    
    @IBOutlet weak var inputToolbar: UIView! {
        didSet {
            inputToolbar.backgroundColor = UIColor.clear
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.centerTextAndImage(spacing: 10.0)
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
        self.btnBack.addTarget(self, action: #selector(sideBarBackClick), for: .touchUpInside)
        self.btnSendMessage.addTarget(self, action: #selector(sendMessageWithoutAttachment), for: .touchUpInside)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
        
        debugPrint("DictGetValue :: ", DictGetValue) // communtyId //
        
        imgProfile.makeRounded()
        self.imgProfile.layer.borderWidth = 2
        
        self.textView.layer.cornerRadius = 5.0
        self.textView.clipsToBounds = true

        DispatchQueue.main.async {
            self.lblName.text = self.DictGetValue["name"]as?String ?? ""
            self.lblTitle.text = self.DictGetValue["content_en"]as?String ?? ""
            self.lblLikes.text = "\(self.DictGetValue["TotalLike"]as?Int ?? 0)"
            self.lblComment.text = "\(self.DictGetValue["TotalComment"]as?Int ?? 0)"
            self.imgProfile.dowloadFromServer(link:self.DictGetValue["image"]as?String ?? "", contentMode: .scaleToFill)
            self.img.dowloadFromServer(link:self.DictGetValue["image"]as?String ?? "\(UIImage(named: "logo_300")!)", contentMode: .scaleToFill)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var keyboardHeight = UIScreen.main.bounds.height - endFrame.origin.y
            if #available(iOS 11, *) {
                if keyboardHeight > 0 {
                    keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                }
            }
            textViewBottomConstraint.constant = keyboardHeight + 8
            view.layoutIfNeeded()
        }
    }
    
    @objc func sideBarBackClick() {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TimelineVC") as? TimelineVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    
    @IBAction func btnCommentClickMe(_ sender : UIButton){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CommentListingVC") as? CommentListingVC
        vc?.DictIndexGetValue = DictGetValue
        self.navigationController?.pushViewController(vc!, animated: false)
    }


    @objc func sendMessageWithoutAttachment() {
        if(textView.text == ""){
            let alert = UIAlertController(title: "Alert", message: "Enter you comment", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.callAddCommentAPI()
        }
    }
    
    
    func callAddCommentAPI(){
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]

                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let values = ["action":"addcomment","postId":DictGetValue["communtyId"]as?Int ?? 0,"userId":String(dict["userId"]as? Int ?? 0),"comment":self.textView.text ?? ""]as [String : Any]as [String : Any]
                print("Values \(values)")
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                
                AF.request(request)
                    .responseJSON { response in
                        MBProgressHUD.hide(for:self.view, animated: true)
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            // self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                let strStatus = dict["status"]as? String ?? ""
                                let alertController = UIAlertController(title: "Alert", message:dict["msg"]as? String ?? "", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    if strStatus != "Fails"  { // msg
                                        self.textView.text = ""
//                                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TimelineVC") as? TimelineVC
//                                        self.navigationController?.pushViewController(vc!, animated: false)
                                    }
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                    }
                }
            }
        }
}


extension CommentsVC: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}


