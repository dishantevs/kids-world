//
//  DashboardVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import AVFoundation

class DashboardVC: UIViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnMenu: UIButton!
    
    
    @IBOutlet weak var viewTop:UIView!

    @IBOutlet weak var btnGift: UIView!
    @IBOutlet weak var btnVideos: UIView!
    @IBOutlet weak var btnWorkbook: UIView!
    @IBOutlet weak var btnFriends: UIView!
    @IBOutlet weak var btnTimeline: UIView!

    // slider//
    var isOpen = false
    var screenCheck = false
    
//    let dashboardSound = URL(fileURLWithPath: Bundle.main.path(forResource: "dashboard", ofType: "mp3")!)
//    var audioPlayer = AVAudioPlayer()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnMenu.centerTextAndImage(spacing: 10.0)
        imgProfile.makeRounded()
        
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
        self.viewTop.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .bottomToTop)

        self.btnGift.applyGradientBtn(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], viewHeight: Int(btnGift.frame.size.height), viewWidth: Int(btnGift.frame.size.width))

        self.btnVideos.applyGradientBtn(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], viewHeight: Int(btnGift.frame.size.height), viewWidth: Int(btnGift.frame.size.width))

        self.btnWorkbook.applyGradientBtn(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], viewHeight: Int(btnGift.frame.size.height), viewWidth: Int(btnGift.frame.size.width))

        self.btnFriends.applyGradientBtn(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], viewHeight: Int(btnGift.frame.size.height), viewWidth: Int(btnGift.frame.size.width))

        self.btnTimeline.applyGradientBtn(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], viewHeight: Int(btnTimeline.frame.size.height), viewWidth: Int(btnTimeline.frame.size.width))
        
        
        // slider //
        self.btnMenu.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        let directions: [UISwipeGestureRecognizer.Direction] = [.left,.right,.up,.down]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(DashboardVC.handleSwipe1(gesture:)))
            gesture.direction = direction
            self.view?.addGestureRecognizer(gesture)
        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        
        let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
        DispatchQueue.main.async {
            self.lblName.text = dict["fullName"]as?String ?? ""
            self.lblPhone.text = dict["contactNumber"]as?String ?? ""
            self.lblEmail.text = dict["email"]as?String ?? ""
            self.imgProfile.dowloadFromServer(link:dict["image"]as?String ?? "", contentMode: .scaleToFill)
        }
        
//        do {
//             audioPlayer = try AVAudioPlayer(contentsOf: dashboardSound)
//             audioPlayer.play()
//        } catch {
//        }
    }
    
//    @objc func methodOfReceivedNotification(notification: Notification){
//        audioPlayer.stop()
//    }
    
    
    @IBAction func btnGiiftClickMe(_ sender : UIButton) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "GiftsListingVC") as? GiftsListingVC
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    @IBAction func btnVideosClickMe(_ sender : UIButton) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "VideosListingVC") as? VideosListingVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }

    @IBAction func btnWorkBookClickMe(_ sender : UIButton) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WorkbookListingVC") as? WorkbookListingVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @IBAction func btnFriendsClickMe(_ sender : UIButton) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChatFriendsListingVC") as? ChatFriendsListingVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }

    @IBAction func btnTimeLineClickMe(_ sender : UIButton) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TimelineVC") as? TimelineVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }

    // Left Slider //
    @objc func handleSwipe1(gesture: UISwipeGestureRecognizer) {
        print(gesture.direction)
        switch gesture.direction {

        case UISwipeGestureRecognizer.Direction.left:
            isOpen = false
            if screenCheck == true{
                let viewMenuBack : UIView = view.subviews.last!
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    var frameMenu : CGRect = viewMenuBack.frame
                    frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                    viewMenuBack.frame = frameMenu
                    viewMenuBack.layoutIfNeeded()
                    viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
                    
                })
                screenCheck = false
            }else{
            }
            print("left swipe")
        case UISwipeGestureRecognizer.Direction.right:
            print("right swipe")
            isOpen = true
            if screenCheck == false{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let menuVC  = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController
                self.view.addSubview(menuVC!.view)
                self.addChild(menuVC!)
                menuVC!.view.layoutIfNeeded()
                
                menuVC!.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
                
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    menuVC!.view.frame=CGRect(x: 0, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
                }, completion:nil)
                screenCheck = true
            }else{
        }
        default:
            print("other swipe")
        }
    }
    @objc func sideBarMenuClick() {
        if(!isOpen){
            screenCheck = true
            isOpen = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController
            self.view.addSubview(menuVC!.view)
            self.addChild(menuVC!)
            menuVC!.view.layoutIfNeeded()
            
            menuVC!.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC!.view.frame=CGRect(x: 0, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
            }, completion:nil)
            
        }else if(isOpen){
            screenCheck = false
            isOpen = false
            let viewMenuBack : UIView = view.subviews.last!
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
        }
    }
}
