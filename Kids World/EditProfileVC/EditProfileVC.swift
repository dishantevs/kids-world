//
//  HelpVC.swift
//  Kids World
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

class EditProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblEmail:UILabel!
    @IBOutlet weak var txtNumber:UITextField!
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var lblDOB:UILabel!
    @IBOutlet weak var txtAddress:UITextField!
    @IBOutlet weak var lblGender:UILabel!
    
    @IBOutlet weak var txtGaurdianName: UITextField!
    @IBOutlet weak var txtSchoolName: UITextField!
    @IBOutlet weak var txtLevelName: UITextField!

    
    @IBOutlet weak var ImgView: UIImageView!
    
    @IBOutlet weak var ImgView1: UIImageView!
    @IBOutlet weak var ImgView11: UIImageView!

    @IBOutlet weak var ImgView2: UIImageView!
    @IBOutlet weak var ImgView22: UIImageView!

    @IBOutlet weak var ImgView3: UIImageView!
    @IBOutlet weak var ImgView33: UIImageView!
    
    var responseDictionary:[String:Any] = [:]
    var dataDictionary:[String:Any] = [:]
    var status:String?
    var msg:String?
    
    var dict: [String : Any] = [:]
    var imagePicker :UIImagePickerController?
    var photo1:UIImage?
    
    var imageData:Data?
    var imageData1:Data?
    var imageData2:Data?
    var imageData3:Data?
    
    var imageData11:Data?
    var imageData22:Data?
    var imageData33:Data?
    
    var UploadIdProofTag = Int()
    var UploadResidencyTag = Int()
    var UploadReportTag = Int()
    
    var UploadIdProofTagFront = Int()
    var UploadResidencyTagFront = Int()
    var UploadReportTagFront = Int()
    
    var imgSetTag = Int()
    var DictGetValue:[String:Any] = [:]
    
    //=====Date Picker====//
    @IBOutlet  var datePicker: UIDatePicker!
    let dateFormatter = DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.centerTextAndImage(spacing: 10.0)
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
        self.ImgView.makeRounded()
        
        self.dict = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
        self.txtName.text = dict["fullName"]as?String ?? ""
        self.txtNumber.text = dict["contactNumber"]as?String ?? ""
        self.txtAddress.text = dict["address"]as?String ?? ""
        
        self.lblEmail.text = dict["email"]as?String ?? ""
        self.lblGender.text = dict["gender"]as?String ?? ""
        self.lblDOB.text = dict["dob"]as?String ?? ""
        
        self.txtGaurdianName.text = dict["gaurdianName"]as?String ?? ""
        self.txtSchoolName.text = dict["elementarySchool"]as?String ?? ""
        self.txtLevelName.text = dict["gradeLevel"]as?String ?? ""
        
        self.ImgView1.dowloadFromServer(link:dict["Proof_of_Identity_back"]as?String ?? "", contentMode: .scaleToFill)
        self.ImgView11.dowloadFromServer(link:dict["Proof_of_Identity"]as?String ?? "", contentMode: .scaleToFill)
      
        self.ImgView2.dowloadFromServer(link:dict["Proof_of_Residency_back"]as?String ?? "", contentMode: .scaleToFill)
        self.ImgView22.dowloadFromServer(link:dict["Proof_of_Residency"]as?String ?? "", contentMode: .scaleToFill)
        
        self.ImgView3.dowloadFromServer(link:dict["Report_Card_back"]as?String ?? "", contentMode: .scaleToFill)
        self.ImgView33.dowloadFromServer(link:dict["Report_Card"]as?String ?? "", contentMode: .scaleToFill)

        self.ImgView.dowloadFromServer(link:dict["image"]as?String ?? "", contentMode: .scaleToFill)

        roundedCorner(img: ImgView11)
        roundedCorner(img: ImgView1)
        roundedCorner(img: ImgView2)
        roundedCorner(img: ImgView22)
        roundedCorner(img: ImgView3)
        roundedCorner(img: ImgView33)
    }
    
    func roundedCorner(img : UIImageView){
        img.layer.cornerRadius = 10.0
        img.layer.borderWidth = 1
        img.layer.borderColor = UIColor.gray.cgColor
        img.layer.masksToBounds = true
    }
    // MARK :- button Action
    @IBAction func btnBackClickMe(_ sender : UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardVC") as? DashboardVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        self.UpdateUserProfile()
    }
    
    func UpdateUserProfile() {
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            self.view.isUserInteractionEnabled = false
            
            let parameters = ["action": "editprofile",
                              "fullName": self.txtName.text ?? "",
                              "contactNumber": self.txtNumber.text ?? "",
                              "address": self.txtAddress.text ?? "",
                              "gaurdianName": txtGaurdianName.text ?? "",
                              "elementarySchool": txtSchoolName.text ?? "",
                              "gradeLevel": txtLevelName.text ?? "",
                              "userId": String(self.dict["userId"]as? Int ?? 0)]as [String : Any]as [String : Any]
            
            var header = HTTPHeaders()
            header = ["Content-type": "multipart/form-data",
                      "Content-Disposition" : "form-data"]
            AF.upload(multipartFormData: { multipartFormData in
                for (key, value) in parameters {
                    if let temp = value as? String {
                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? Int {
                        multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key + "[]"
                            if let string = element as? String {
                                multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                            if let num = element as? Int {
                                let value = "\(num)"
                                multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
                if let img = self.imageData {
                    multipartFormData.append(img, withName:"image", fileName: "image.jpg", mimeType: "image/jpg")
                }
                if let img = self.imageData1 {
                    multipartFormData.append(img, withName:"Proof_of_Identity_back", fileName: "Proof_of_Identity_back.jpg", mimeType: "image/jpg")
                }
                if let img = self.imageData2 {
                    multipartFormData.append(img, withName:"Proof_of_Residency_back", fileName: "Proof_of_Residency_back.jpg", mimeType: "image/jpg")
                }
                if let img = self.imageData3 {
                    multipartFormData.append(img, withName:"Report_Card_back", fileName: "Report_Card_back.jpg", mimeType: "image/jpg")
                }
                //=========New=====//
                if let img = self.imageData11 {
                    multipartFormData.append(img, withName:"Proof_of_Identity", fileName: "Proof_of_Identity.jpg", mimeType: "image/jpg")
                }
                if let img = self.imageData22 {
                    multipartFormData.append(img, withName:"Proof_of_Residency", fileName: "Proof_of_Residency.jpg", mimeType: "image/jpg")
                }
                if let img = self.imageData33 {
                    multipartFormData.append(img, withName:"Report_Card", fileName: "Report_Card.jpg", mimeType: "image/jpg")
                }
            },
            to:BaseUrl, method: .post , headers: header)
            .responseJSON(completionHandler: { (response) in
                print(response)
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.isUserInteractionEnabled = true;
                if let err = response.error {
                    print(err)
                    return
                }
                print("Succesfully uploaded")
                let jsonData = response.data
                self.dismiss(animated:true, completion: nil)
                if (jsonData != nil) {
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.view.isUserInteractionEnabled = true
                    }
                    do {
                        if let json = try JSONSerialization.jsonObject(with: jsonData ?? Data(), options: []) as? [String: Any] {
                            print(json)
                            let strStatus = json["status"]as? String ?? ""
                            
                            let alertController = UIAlertController(title: "Alert", message:json["msg"]as? String ?? "", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                if strStatus != "Fails"  { // msg //
                                    self.dataDictionary = json["data"] as? [String:Any] ?? [:]
                                    let dictLogin : NSDictionary? = self.dataDictionary as NSDictionary
                                    let prefs:UserDefaults = UserDefaults.standard
                                    prefs.set(self.dataDictionary["userId"] as? Int, forKey:"UserID")
                                    prefs.set(self.dataDictionary["fullName"] as? String, forKey:"Name")
                                    prefs.set(self.dataDictionary["email"] as? String, forKey:"Email")
                                    prefs.set(self.dataDictionary["address"] as? String, forKey:"Address")
                                    prefs.set(self.dataDictionary["contactNumber"] as? Int, forKey: "ContactNumber")
                                    prefs.set(dictLogin, forKey: "kAPI_LOGIN_DATA")
                                    prefs.synchronize()
                                    
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardVC") as? DashboardVC
                                    self.navigationController?.pushViewController(vc!, animated: false)
                                }
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                }
            })
        }
    }
    
    @IBAction func btnDOBClickME(_ sender: Any) {
        self.displayAlertControllerDatePicker(title: "Alert", message: "Choose DOB")
    }
    
    @IBAction func btnGenderClickME(_ sender: Any) {
        let actionSheetController: UIAlertController = UIAlertController(title: "Action Sheet", message: "Choose an option!", preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Ok", style: .cancel) { action -> Void in
            print("cancel")
        }
        actionSheetController.addAction(cancelAction)
        let chooseMaleAction: UIAlertAction = UIAlertAction(title: "Male", style: .default) { action -> Void in
            print("Male")
            self.lblGender.text = "Male"
        }
        actionSheetController.addAction(chooseMaleAction)
        let chooseFemaleAction: UIAlertAction = UIAlertAction(title: "Female", style: .default) { action -> Void in
            print("Female")
            self.lblGender.text = "Female"
        }
        actionSheetController.addAction(chooseFemaleAction)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    
    @IBAction func btnPickPhotoClickMe(_ sender : UIButton) {
        imgSetTag = 101
        self.picImageFromGalleyOrCamer()
    }
    @IBAction func btnIDProofClickMe(_ sender : UIButton){
        imgSetTag = 1
        UploadIdProofTag = 2
        self.picImageFromGalleyOrCamer()
    }
    @IBAction func btnResidencyClickMe(_ sender : UIButton){
        imgSetTag = 2
        UploadResidencyTag = 3
        self.picImageFromGalleyOrCamer()
    }
    @IBAction func btnReportClickMe(_ sender : UIButton){
        imgSetTag = 3
        UploadReportTag = 4
        self.picImageFromGalleyOrCamer()
    }
   //================
    @IBAction func btnIDProofFrontClickMe(_ sender : UIButton){
        imgSetTag = 11
        UploadIdProofTagFront = 22
        self.picImageFromGalleyOrCamer()
    }
    @IBAction func btnResidencyFrontClickMe(_ sender : UIButton){
        imgSetTag = 22
        UploadResidencyTagFront = 33
        self.picImageFromGalleyOrCamer()
    }
    @IBAction func btnReportFrontClickMe(_ sender : UIButton){
        imgSetTag = 33
        UploadReportTagFront = 44
        self.picImageFromGalleyOrCamer()
    }

    func picImageFromGalleyOrCamer() {
        let actionSheetController: UIAlertController = UIAlertController(title: "Action Sheet", message: "Choose an option!", preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelAction)
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(takePictureAction)
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Camera Roll", style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(choosePictureAction)
        if let popoverController = actionSheetController.popoverPresentationController {
            popoverController.sourceView = self.ImgView
        }
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width:size.width * heightRatio, height:size.height * heightRatio)
        } else {
            newSize = CGSize(width:size.width * widthRatio,  height:size.height * widthRatio)
        }
        let rect = CGRect(x:0, y:0, width:newSize.width, height:newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 8.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]){
        photo1 = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage //2
        let img = self.resizeImage(image: self.photo1!, targetSize:CGSize(width:100.0 , height:100.0))
        
        if imgSetTag == 101{
            self.ImgView.image = img
            if let image  = ImgView.image,let imageData = image.jpegData(compressionQuality: 8.0) {
                self.imageData = imageData
            }
        }
        
        if imgSetTag == 1{
            ImgView1.image = img
        }
        if imgSetTag == 2{
            ImgView2.image = img
            if let image  = ImgView11.image,let imageData = image.jpegData(compressionQuality: 8.0) {
                self.imageData11 = imageData
            }
        }
        if imgSetTag == 3{
            ImgView3.image = img
        }
        if imgSetTag == 11{
            ImgView11.image = img
            if let image  = ImgView11.image,let imageData = image.jpegData(compressionQuality: 8.0) {
                self.imageData11 = imageData
            }
        }
        if imgSetTag == 22{
            ImgView22.image = img
        }
        if imgSetTag == 33{
            ImgView33.image = img
            if let image  = ImgView33.image,let imageData = image.jpegData(compressionQuality: 8.0) {
                self.imageData33 = imageData
            }
        }
        
        // =========1=========== //
        if(UploadIdProofTag == 2) {
            if let image  = ImgView1.image,let imageData = image.jpegData(compressionQuality: 8.0) {
                self.imageData1 = imageData
            }
        }
    // =========2========== //
        if(UploadResidencyTag == 3) {
            if let image  = ImgView2.image,let imageData = image.jpegData(compressionQuality: 8.0) {
               self.imageData2 = imageData
            }
        }
        if(UploadResidencyTagFront == 33) {
            if let image  = ImgView22.image,let imageData = image.jpegData(compressionQuality: 8.0) {
                self.imageData22 = imageData
            }
        }
   // =========3======== //
        if(UploadReportTag == 4) {
            if let image  = ImgView3.image,let imageData = image.jpegData(compressionQuality: 8.0) {
                self.imageData3 = imageData
            }
        }
        self.dismiss(animated:true, completion: nil)
    }
}


/** --------------------------------------------------------
*        Date Picker View defined by Targets.
*  --------------------------------------------------------
*/
extension EditProfileVC {
    func displayAlertControllerDatePicker(title:String, message:String) {
        datePicker  = UIDatePicker(frame: CGRect(x: 10, y: 65, width: self.view.frame.width - 20, height: 150))
        datePicker.addTarget(self, action: #selector(EditProfileVC.dueDateChanged(_:)), for: UIControl.Event.valueChanged)
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let editRadiusAlert = UIAlertController(title:title , message: message, preferredStyle: UIAlertController.Style.actionSheet)
        let height:NSLayoutConstraint = NSLayoutConstraint(item: editRadiusAlert.view as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 330)
        editRadiusAlert.view.addConstraint(height);
        editRadiusAlert.view.addSubview(datePicker)
        
        editRadiusAlert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
        editRadiusAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: cancelClickEvent))
        self.present(editRadiusAlert, animated: true)

        datePicker.datePickerMode = UIDatePicker.Mode.date
        datePicker.minimumDate = Calendar.current.date(byAdding: .month, value: -1116, to: Date()) // 90*12+36 = 1116  (90 yr)
        datePicker.maximumDate = Calendar.current.date(byAdding: .month, value: -36, to: Date()) // 3*12 = 36 (3 yr)
    }
    
    @IBAction func dueDateChanged(_ sender: Any) {
        self.txtName.resignFirstResponder()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strDate = dateFormatter.string(from: (sender as AnyObject).date)
        lblDOB.text = strDate
        print(lblDOB.text as Any)
    }
    func cancelClickEvent(action: UIAlertAction) {
    }
}
