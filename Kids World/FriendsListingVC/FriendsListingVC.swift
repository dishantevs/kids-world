//
//  FriendsListingVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import MBProgressHUD
import SwiftyJSON

class FriendsListingVC: UIViewController ,UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate {
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnMenu: UIButton!
    // slider//
    var isOpen = false
    var screenCheck = false
    
    
    //sam
    var currentPageNumber:Int = 1
    var totalPage: Int = 0
    var arrJSON  = [AnyObject]()
    var fetchNewArray: [[String:Any]] = [[:]]
    
    var responseDictionary:[String:Any] = [:]
    var arrListOfAllMyOrders:NSMutableArray! = []
    
    @IBOutlet weak var viewSearch: UIView! {
        didSet {
            viewSearch.isHidden = false
        }
    }
    
    @IBOutlet weak var txtSearch : UITextField! {
        didSet {
            txtSearch.delegate = self
        }
    }
    
    @IBOutlet weak var btnSearch:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbleView.isHidden = true
        lblNoRecord.isHidden = false
        
        btnMenu.centerTextAndImage(spacing: 10.0)
        
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
        // slider //
        self.btnMenu.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        let directions: [UISwipeGestureRecognizer.Direction] = [.left,.right,.up,.down]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(DashboardVC.handleSwipe1(gesture:)))
            gesture.direction = direction
            self.view?.addGestureRecognizer(gesture)
        }
        
        self.btnSearch.addTarget(searchClickMethod, action: #selector(searchClickMethod), for: .touchUpInside)
        
        self.arrJSON.removeAll()
        self.tbleView.isHidden = true
        self.getFriendsListingAPI(pageNo : currentPageNumber, str_search_key: "")
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @objc func searchClickMethod() {
        self.getFriendsListingAPI(pageNo : currentPageNumber, str_search_key: String(self.txtSearch.text!))
    }
    func getFriendsListingAPI(pageNo: Int,str_search_key:String) {
        
        if isInternetAvailable() == false {
            
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true)
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!"
            
            if let apiString = URL(string:BaseUrl) {
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                let values = ["action"      : "friendlist",
                              "userId"      : String(dict["userId"]as? Int ?? 0),
                              "status"      : "0",
                              "keyword":String(str_search_key)] as [String : Any]
                
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                
                print("Values \(values)")
                
                AF.request(request)
                    .responseJSON { response in
                        // do whatever you want here
                        MBProgressHUD.hide(for:self.view, animated: true)
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "No more record found", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            DispatchQueue.main.async {
                                print("This is run on the main queue, after the previous code in outer block")
                            }
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            
                            if let dict = responseObject as? [String:Any] {
                                let status = dict["status"] as? String
                                if(status != "Fails") {
                                    self.fetchNewArray.removeAll()
                                    if (self.currentPageNumber == 1) {
                                        self.arrJSON  = (dict["data"] as AnyObject) as! [AnyObject]
                                    } else {
                                        self.fetchNewArray  = ((dict["data"] as AnyObject) as! [AnyObject] as NSArray) as! [[String : Any]]
                                    }
                                    if self.arrJSON.count != 0 {
                                        
                                        if (self.currentPageNumber == 1) {
                                            
                                            self.tbleView.isHidden = false
                                            self.lblNoRecord.isHidden = true
                                            self.tbleView.reloadData()
                                            
                                        } else {
                                            
                                            if (self.fetchNewArray.count != 0) {
                                                self.didFinishRecordsRequest(result: (self.fetchNewArray) as NSArray, pageNo: self.currentPageNumber)
                                            } else {
                                                self.currentPageNumber -= 1
                                            }
                                        }
                                        
                                    } else {
                                        
                                        self.lblNoRecord.isHidden = false
                                        self.tbleView.isHidden = true
                                        
                                    }
                                    
                                } else {
                                    
                                    self.lblNoRecord.isHidden = false
                                    self.tbleView.isHidden = true
                                    
                                }
                                
                            }
                        }
                    }
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if(currentPageNumber != 1) {
            
            let lastItem = self.arrJSON.count - 1
            
            if indexPath.row == lastItem {
                
                print("IndexRow :: \(indexPath.row)")
                self.currentPageNumber += 1
                self.getFriendsListingAPI(pageNo: self.currentPageNumber, str_search_key: "")
                
            }
            
        }
        
    }
    
    func didFinishRecordsRequest(result:NSArray, pageNo:Int) {
        
        if (pageNo != 0) {
            
            self.arrJSON += result as Array<AnyObject>
            
        } else {
            
            self.arrJSON.removeAll()
            self.arrJSON = result.mutableCopy() as! [AnyObject]
            
        }
        
        self.totalPage = Int(self.arrJSON.count)
        self.tbleView.reloadData()
        
    }
    
    // MARK :- Tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrJSON.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:FriendListTableCell = tableView.dequeueReusableCell(withIdentifier: "FriendListTableCell") as! FriendListTableCell
        
        let dict : [String :Any] = arrJSON[indexPath.row] as! [String : Any]
        print(dict as Any)
        
        cell.lblTitle.text = (dict["userName"]as?String ?? "").uppercased()
        cell.lblAddress.text = (dict["userAddress"]as?String ?? "").capitalized
        
        let dicttt : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
        let idUser = dicttt["userId"]as? Int ?? 0
        let userid = dict["userId"]as?Int ?? 0
        
        if(idUser == userid) {
            
            cell.lblStatus.text = "Request Sent"
            
        } else {
            
            cell.lblStatus.text = "Request Receive"
            
        }
        
        cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.height / 2
        cell.imgProfile.clipsToBounds = true
        
        cell.imgProfile.dowloadFromServer(link:dict["profile_picture"]as?String ?? "", contentMode: .scaleToFill)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FriendsUserProfileDetailsVC") as? FriendsUserProfileDetailsVC
        push?.DictGetValue = self.arrJSON[indexPath.row] as? [String : Any] ?? [:]
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    
    
    // Left Slider //
    @objc func handleSwipe1(gesture: UISwipeGestureRecognizer) {
        print(gesture.direction)
        switch gesture.direction {
            
        case UISwipeGestureRecognizer.Direction.left:
            isOpen = false
            if screenCheck == true {
                let viewMenuBack : UIView = view.subviews.last!
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    var frameMenu : CGRect = viewMenuBack.frame
                    frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                    viewMenuBack.frame = frameMenu
                    viewMenuBack.layoutIfNeeded()
                    viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
                    
                })
                screenCheck = false
            }else{
            }
            print("left swipe")
        case UISwipeGestureRecognizer.Direction.right:
            print("right swipe")
            isOpen = true
            if screenCheck == false {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let menuVC  = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController
                self.view.addSubview(menuVC!.view)
                self.addChild(menuVC!)
                menuVC!.view.layoutIfNeeded()
                
                menuVC!.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
                
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    menuVC!.view.frame=CGRect(x: 0, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
                }, completion:nil)
                screenCheck = true
                
            } else {
                
            }
            
        default:
            print("other swipe")
        }
    }
    
    @objc func sideBarMenuClick() {
        
        if(!isOpen) {
            
            screenCheck = true
            isOpen = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController
            self.view.addSubview(menuVC!.view)
            self.addChild(menuVC!)
            menuVC!.view.layoutIfNeeded()
            
            menuVC!.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC!.view.frame=CGRect(x: 0, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
            }, completion:nil)
            
        } else if(isOpen) {
            
            screenCheck = false
            isOpen = false
            let viewMenuBack : UIView = view.subviews.last!
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
            
        }
    }
    
}


class FriendListTableCell : UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    
}
