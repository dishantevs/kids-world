//
//  HelpVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD


class FriendsUserProfileDetailsVC: UIViewController {
    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    
    @IBOutlet weak var lblNameTop:UILabel!
    @IBOutlet weak var lblAddressTop:UILabel!
    
    
    @IBOutlet weak var lblEmail:UILabel!
    @IBOutlet weak var lblNumber:UILabel!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDOB:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblGender:UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    
    var responseDictionary:[String:Any] = [:]
    var dataDictionary:[String:Any] = [:]
    var status:String?
    var msg:String?
    
    
    @IBOutlet weak var btnAccpet: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    
    
    var DictGetValue:[String:Any] = [:]
    override func viewDidLoad() {
        
        super.viewDidLoad()
        btnBack.centerTextAndImage(spacing: 10.0)
        
        // 24 109 160
        self.viewNavBG.backgroundColor = UIColor.init(red: 24.0/255.0, green: 109.0/255.0, blue: 160.0/255.0, alpha: 1)
        // self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
        self.lblNameTop.text = DictGetValue["userName"]as? String ?? ""
        self.lblAddressTop.text = DictGetValue["userAddress"]as? String ?? ""
        self.imgProfile.dowloadFromServer(link:DictGetValue["profile_picture"]as?String ?? "", contentMode: .scaleToFill)
        self.imgProfile.makeRoundedCorn()
        
        let dicttt : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
        let idUser = dicttt["userId"]as? Int ?? 0
        let userid = DictGetValue["userId"]as?Int ?? 0
        
        if(idUser == userid) {
            
            btnAccpet.isHidden = true
            btnReject.isHidden = true
            // hiden
            
        } else {
            
            // Show
            btnAccpet.isHidden = false
            btnReject.isHidden = false
            
        }
        
        btnAccpet.layer.cornerRadius = 10
        btnAccpet.layer.borderWidth = 1
        btnAccpet.layer.borderColor = UIColor.gray.cgColor
        
        btnReject.layer.cornerRadius = 10
        btnReject.layer.borderWidth = 1
        btnReject.layer.borderColor = UIColor.gray.cgColor
        
        self.getUserProfile()
    }
    
    
    // MARK :- button Action
    @IBAction func btnBackClickMe(_ sender : UIButton){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FriendsListingVC") as? FriendsListingVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @IBAction func btnAcceptAction(_ sender: UIButton) {
        self.getAccept_RejectProfile(TypeStatus: "1") // accept
    }
    @IBAction func btnRejectAction(_ sender: UIButton) {
        self.getAccept_RejectProfile(TypeStatus: "2") // reject
    }
    
    func getAccept_RejectProfile(TypeStatus : String) {
        
        if isInternetAvailable() == false {
            
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        else {
            
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                if(TypeStatus == "1") {
                    
                    let values = ["action":"sendrequest",
                                  "userId":String(dict["userId"]as? Int ?? 0),
                                  "profileId":DictGetValue["profileId"]as? Int ?? 0,
                                  "status":TypeStatus] as [String : Any]
                    
                    print("Values \(values)")
                    request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                    
                } else {
                    
                    let values = ["action"      : "sendrequest",
                                  "userId"      : String(dict["userId"]as? Int ?? 0),
                                  "profileId"   : DictGetValue["profileId"]as? Int ?? 0,
                                  "status"      : TypeStatus] as [String : Any]
                    
                    print("Values \(values)")
                    request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                }
                AF.request(request)
                    .responseJSON { response in
                        switch response.result {
                        case .failure(let error):
                            MBProgressHUD.hide(for:self.view, animated: true)
                            
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                                MBProgressHUD.hide(for:self.view, animated: true)
                            }
                            // Values ["blockBy": "", "userId": "98", "action": "sendrequest", "profileId": 90, "status": "2"]
                        case .success(let responseObject):
                            DispatchQueue.main.async {
                                print("This is run on the main queue, after the previous code in outer block")
                            }
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                self.responseDictionary = dict
                            }
                            self.status = self.responseDictionary["status"] as? String
                            let alertController = UIAlertController(title: "Alert", message: self.responseDictionary["msg"] as? String, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                if self.status == "success" {
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FriendsListingVC") as? FriendsListingVC
                                    self.navigationController?.pushViewController(vc!, animated: false)
                                }
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
            }
            
        }
    }
    
    func getUserProfile() {
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let values = ["action":"profile","userId":DictGetValue["profileId"]as? Int ?? 0] as [String : Any]
                print("Values \(values)")
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                AF.request(request)
                    .responseJSON { response in
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            DispatchQueue.main.async {
                                print("This is run on the main queue, after the previous code in outer block")
                            }
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                self.responseDictionary = dict
                            }
                            self.status = self.responseDictionary["status"] as? String
                            if self.status == "Fails"{
                                let alertController = UIAlertController(title: "Alert", message: self.responseDictionary["msg"] as? String, preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    print("you have pressed the Ok button");
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion:nil)
                            } else {
                                let dict = self.responseDictionary["data"] as? [String: Any]
                                self.lblEmail.text = dict?["email"]as? String ?? ""
                                self.lblName.text = dict?["fullName"]as? String ?? ""
                                self.lblNumber.text = dict?["contactNumber"] as? String
                                self.lblGender.text = dict?["gender"]as? String ?? ""
                                self.lblDOB.text = dict?["dob"]as? String ?? ""
                                self.lblAddress.text = dict?["address"]as? String ?? ""
                                self.imgProfile.dowloadFromServer(link: dict?["document"]as? String ?? "", contentMode: .scaleToFill)
                            }
                        }
                    }
            }
        }
    }
}
