//
//  GiftsListingVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD

class GiftsListingVC: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    
    @IBOutlet weak var collView: UICollectionView!
    var responseDictionary:[String:Any] = [:]
    var responseArray:[[String:Any]] = [[:]]
    
    // slider//
    var isOpen = false
    var screenCheck = false
    
    var arrQuestionAnswerShow : NSMutableArray = NSMutableArray()
    
    var checkResponseTag = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnMenu.centerTextAndImage(spacing: 10.0)
        btnCart.centerTextAndImage(spacing: 10.0)
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
        self.btnCart.addTarget(self, action: #selector(sideBarWishListingClick), for: .touchUpInside)
        
        // slider //
        self.btnMenu.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        let directions: [UISwipeGestureRecognizer.Direction] = [.left,.right,.up,.down]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(DashboardVC.handleSwipe1(gesture:)))
            gesture.direction = direction
            self.view?.addGestureRecognizer(gesture)
        }
        
        self.collView.isHidden = true
        self.callGiftAPI()
    }
    
    @objc func sideBarWishListingClick() {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WishlistListingVC") as? WishlistListingVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
    
    func callGiftAPI(){
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let values = ["action":"giftist"]as [String : Any]as [String : Any]
                print("Values \(values)")
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                AF.request(request)
                    .responseJSON { response in
                        // do whatever you want here
                        MBProgressHUD.hide(for:self.view, animated: true)
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            // self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                self.responseArray = dict["data"] as? [[String : Any]] ?? [[:]]
                                print(self.responseArray)
                                if(self.responseArray.count != 0){
                                    self.checkResponseTag = 2
                                    for index in 0..<self.responseArray.count {
                                        let paraInfo:NSMutableDictionary = NSMutableDictionary()
                                        paraInfo.setValue(0, forKey: "isHide")
                                        self.arrQuestionAnswerShow.insert(paraInfo, at: index)
                                    }
                                    self.collView.isHidden = false
                                    self.collView.reloadData()
                                }else{
                                    self.collView.isHidden = true
                                }
                            }
                        }
                    }
            }
        }
    }
    
    //MARK: Table and Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.responseArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: GiftCollectionCell = collView.dequeueReusableCell(withReuseIdentifier: "GiftCollectionCell", for: indexPath) as! GiftCollectionCell
        
        let dict : [String :Any] = self.responseArray[indexPath.row]
//        cell.lblTxt.text = dict["name"]as?String
        if dict["points"]as?String ?? "" != ""{
            cell.lblTxt.text = "\(dict["name"]as?String ?? "")\n\("Points: \(dict["points"]as?String ?? "")")"
//            cell.lblPonit.text = "Points: \(dict["points"]as?String ?? "")"
        }else{
            cell.lblTxt.text = dict["name"]as?String
        }
        cell.lblTxt.halfTextColorChange(fullText: cell.lblTxt.text ?? "", changeText: "Points: \(dict["points"]as?String ?? "")")
        
        cell.img.dowloadFromServer(link:dict["image"]as?String ?? "", contentMode: .scaleToFill)
        
        cell.btnAddWishList.tag = indexPath.row
        cell.btnAddWishList.addTarget(self, action: #selector(self.tapCheckHandle(_:)), for: .touchUpInside)
        
        if(checkResponseTag == 2){
            let dictLocal : [String :Any] = self.arrQuestionAnswerShow[indexPath.row] as? [String : Any] ?? [:]
            if(dictLocal["isHide"]as?Int ?? 0 == 0) {
                cell.btnAddWishList.setImage(#imageLiteral(resourceName: "heartGray"), for: .normal)
                cell.btnAddWishList.isUserInteractionEnabled = true
            }else{
                cell.btnAddWishList.setImage(#imageLiteral(resourceName: "wishilist"), for: .normal)
                cell.btnAddWishList.isUserInteractionEnabled = false
            }
        }
        return cell
    }
    
    @objc func tapCheckHandle(_ sender : UIButton) {
        let dict : [String :Any] = self.responseArray[sender.tag]
        self.callAddWishListAPI(giftId: dict["giftId"]as? Int ?? 0,senderTag : sender.tag)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width  = (collectionView.frame.width)/2
        let height  = width + 20
        return CGSize(width: width, height: height)
    }
    
    func callAddWishListAPI( giftId : Int , senderTag : Int ){
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
            if let apiString = URL(string:BaseUrl) {
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let values = ["action":"addwishlist","giftId":giftId,"userId":String(dict["userId"]as? Int ?? 0)]as [String : Any]as [String : Any]
                print("Values \(values)")
                
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                AF.request(request)
                    .responseJSON { response in
                        // do whatever you want here
                        MBProgressHUD.hide(for:self.view, animated: true)
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                let alertController = UIAlertController(title: "Alert", message: dict["msg"]as? String ?? "", preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                    if(dict["status"]as? String ?? "" != "Fails"){
                                        for index in 0..<self.arrQuestionAnswerShow.count {
                                            if(index == senderTag) {
                                                self.arrQuestionAnswerShow.removeObject(at: index)
                                                let paraInfo:NSMutableDictionary = NSMutableDictionary()
                                                paraInfo.setValue(1, forKey: "isHide")
                                                self.arrQuestionAnswerShow.insert(paraInfo, at: index)
                                            }
                                        }
                                        self.collView.reloadData()
                                    }
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                    }
            }
        }
    }
    
    
    // Left Slider //
    @objc func handleSwipe1(gesture: UISwipeGestureRecognizer) {
        print(gesture.direction)
        switch gesture.direction {
        
        case UISwipeGestureRecognizer.Direction.left:
            isOpen = false
            if screenCheck == true{
                let viewMenuBack : UIView = view.subviews.last!
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    var frameMenu : CGRect = viewMenuBack.frame
                    frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                    viewMenuBack.frame = frameMenu
                    viewMenuBack.layoutIfNeeded()
                    viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
                    
                })
                screenCheck = false
            }else{
            }
            print("left swipe")
        case UISwipeGestureRecognizer.Direction.right:
            print("right swipe")
            isOpen = true
            if screenCheck == false{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let menuVC  = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController
                self.view.addSubview(menuVC!.view)
                self.addChild(menuVC!)
                menuVC!.view.layoutIfNeeded()
                
                menuVC!.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
                
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    menuVC!.view.frame=CGRect(x: 0, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
                }, completion:nil)
                screenCheck = true
            }else{
            }
        default:
            print("other swipe")
        }
    }
    @objc func sideBarMenuClick() {
        if(!isOpen){
            screenCheck = true
            isOpen = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController
            self.view.addSubview(menuVC!.view)
            self.addChild(menuVC!)
            menuVC!.view.layoutIfNeeded()
            
            menuVC!.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC!.view.frame=CGRect(x: 0, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
            }, completion:nil)
            
        }else if(isOpen){
            screenCheck = false
            isOpen = false
            let viewMenuBack : UIView = view.subviews.last!
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
        }
    }
}

class GiftCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTxt: UILabel!
    @IBOutlet weak var lblPonit: UILabel!
    @IBOutlet weak var btnAddWishList: UIButton!
    
    override func awakeFromNib(){
        super.awakeFromNib()
        img.makeRoundedCorn()
    }
}
extension UILabel {
    func halfTextColorChange (fullText : String , changeText : String ) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue , range: range)
        self.attributedText = attribute
    }
}
