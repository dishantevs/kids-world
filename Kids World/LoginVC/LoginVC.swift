//
//  LoginVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import MBProgressHUD

class LoginVC: UIViewController {
    
    @IBOutlet weak var viewBG:UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnBack: UIButton!

    var responseDictionary:[String:Any] = [:]
    var dataDictionary:[String:Any] = [:]
    var status: String?
    var msg: String?

    
    @IBOutlet weak var btnCheckBox:UIButton!

    @IBOutlet weak var viewNavBG:UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        roundedShadowBorderOnView(view: viewBG)
        btnBack.centerTextAndImage(spacing: 10.0)
        btnCheckBox.centerTextAndImage(spacing: 8.0)

        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
//        txtEmail.text = "raj12@gmail.com"
//        txtPassword.text = "123456"
    }

    
    @IBAction func btn_box(sender: UIButton) {
        if (btnCheckBox.isSelected == true) {
            btnCheckBox.isSelected = false
            btnCheckBox.setImage(#imageLiteral(resourceName: "uncheckB"), for: .normal)
        }
        else {
            btnCheckBox.setImage(#imageLiteral(resourceName: "checkB"), for: .normal)
            btnCheckBox.isSelected = true
        }
        
//        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TermsConditionVC")
//        self.navigationController?.pushViewController(push, animated: true)
    }
    
    // MARK:- VALIDATION BEFORE REGISTRATION -
    @IBAction func btnLoginClickMe(_ sender : UIButton){
        if String(txtEmail.text!) == "" {
            self.validationAlertPopup(strAlertTitle: "Alert", strMessage: "Email Address ", strMessageAlert: "should not be Empty")
        } else if String(txtPassword.text!) == "" {
            self.validationAlertPopup(strAlertTitle: "Alert", strMessage: "Password ", strMessageAlert: "should not be Empty")
        } else {
            self.loginAPI()
        }
    }
    
    func loginAPI() {
        if isInternetAvailable() == false {
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            if(txtEmail.text!.isEqual("") ||  txtPassword.text!.isEqual("")) {
                let alertController = UIAlertController(title: "Alert", message: "Please fill the vacant field", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    print("you have pressed the Ok button");
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion:nil)
            }else{
                let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
                spinnerActivity.label.text = "Loading";
                spinnerActivity.detailsLabel.text = "Please Wait!!";
                
                let strLat = UserDefaults.standard.string(forKey: "latitude") ?? "27.877735"
                let strLong = UserDefaults.standard.string(forKey: "longitude") ?? "79.9163692"

                if let apiString = URL(string: BaseUrl) {
                    var request = URLRequest(url:apiString)
                    request.httpMethod = "POST"
                    request.setValue("application/json",  forHTTPHeaderField: "Content-Type")
                    let values = ["action":"login",
                                  "email":txtEmail.text ?? "",
                                  "password":txtPassword.text ?? "",
                                  "device":"IOS",
                                  "latitude": strLat,
                                  "longitude": strLong,
                                  "deviceToken":UserDefaults.standard.string(forKey: "deviceFirebaseToken") ?? "APA91bFPtZDwmdAkbSUhcwqFTh2Y1tcmYFPcRFXQq0StFmkMIaM5V3_Ku8j"]as [String : Any]as [String : Any]
                    
                    print("Values \(values)")
                    request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                    AF.request(request).responseJSON { response in
                        // do whatever you want here
                        switch response.result {
                        case .failure(let error):
                            MBProgressHUD.hide(for:self.view, animated: true)
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                                let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            DispatchQueue.main.async {
                                print("This is run on the main queue, after the previous code in outer block")
                            }
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                self.responseDictionary = dict
                            }
                            self.status = self.responseDictionary["status"] as? String
                            if self.status == "Fails" {
                                let alertController = UIAlertController(title: "Alert", message:self.responseDictionary["msg"] as? String, preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    print("you have pressed the Ok button");
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion:nil)
                            }else{
                                self.dataDictionary = self.responseDictionary["data"] as! [String:Any]
                                let dictLogin : NSDictionary? = self.dataDictionary as NSDictionary
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set(self.dataDictionary["userId"] as? Int, forKey:"UserID")
                                prefs.set(self.dataDictionary["fullName"] as? String, forKey:"Name")
                                prefs.set(self.dataDictionary["email"] as? String, forKey:"Email")
                                prefs.set(self.dataDictionary["address"] as? String, forKey:"Address")
                                prefs.set(self.dataDictionary["contactNumber"] as? String, forKey: "ContactNumber")
                                prefs.set(dictLogin, forKey: "kAPI_LOGIN_DATA")
                                prefs.synchronize()
                                
                                UserDefaults.standard.setValue("1", forKey: "loginStatus")
                                UserDefaults.standard.setValue("1", forKey: "profileUpdate")
                                
                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardVC") as? DashboardVC
                                self.navigationController?.pushViewController(vc!, animated: false)
                            }
                        }
                    }
                }
            }
        }
    }
    


    
    // MARK :- button Action
    @IBAction func btnBackClickMe(_ sender : UIButton){
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
//        self.navigationController?.pushViewController(vc!, animated: false)
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func btnRegistrationClickMe(_ sender : UIButton){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegisterVC") as? RegisterVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    @IBAction func btnForgotClickMe(_ sender : UIButton){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ForgotPasswordVC") as? ForgotPasswordVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
}
