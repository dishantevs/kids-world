//
//  RegisterVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import MBProgressHUD
import PhotosUI

class RegisterVC: UIViewController , UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtGaurdianName: UITextField!
    @IBOutlet weak var txtSchoolName: UITextField!
    @IBOutlet weak var txtLevelName: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtContactNumber: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
//    @IBOutlet weak var txtDocName: UITextField!
    
//    @IBOutlet weak var txtIdProofName: UITextField!
//    @IBOutlet weak var txtResidencyName: UITextField!
//    @IBOutlet weak var txtReportName: UITextField!

//    var UploadDocTag = Int()
    var UploadIdProofTag = Int()
    var UploadResidencyTag = Int()
    var UploadReportTag = Int()
    
    var UploadIdProofTagFront = Int()
    var UploadResidencyTagFront = Int()
    var UploadReportTagFront = Int()

    var imgSetTag = Int()


    @IBOutlet weak var btnAddGrades:UIButton!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    @IBOutlet weak var btnHouse: UIButton!
    @IBOutlet weak var btnApartment: UIButton!
    @IBOutlet weak var btnOthers: UIButton!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var ImgView: UIImageView!
    @IBOutlet weak var ImgView1: UIImageView!
    
    @IBOutlet weak var ImgView2: UIImageView!
    @IBOutlet weak var ImgView22: UIImageView!

    @IBOutlet weak var ImgView3: UIImageView!
    @IBOutlet weak var ImgView33: UIImageView!

    
    var imagePicker :UIImagePickerController?
    var photo1:UIImage?
    
    var imageData1:Data?
    var imageData2:Data?
    var imageData3:Data?
    
    var imageData11:Data?
    var imageData22:Data?
    var imageData33:Data?
    
    var strLat = String()
    var strLong = String()
    var strGender = String()
    
    var tagCamera_Gallery = Int()
    
    var responseDictionary:[String:Any] = [:]
    var dataDictionary:[String:Any] = [:]
    var status:String?
    var msg:String?
    
    @IBOutlet weak var btnCheckBox:UIButton!

    //=====Date Picker====//
    @IBOutlet  var datePicker: UIDatePicker!
    let dateFormatter = DateFormatter()
    
    @IBOutlet weak var viewNavBG:UIView!
    
    
    let InformationTxt = "Proof of residency is for the safety of the child and for the secured shipment of earned rewards every month addressed to the child. Approved examples  could be a letter of any kind addressed to the child with the address provided clearly visible  (I.e. boy/girl scouts letter, approval letter of any kind, etc.), or a lease/mortgage with the child and guardian listed on the document. Homelessness shelter letters of any kind will be accepted as well."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roundedShadowBorderOnView(view: viewBG)
        btnCheckBox.centerTextAndImage(spacing: 8.0)

        btnBack.centerTextAndImage(spacing: 10.0)
        strGender = ""
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
        roundedCorner(img: ImgView)
        roundedCorner(img: ImgView1)
        roundedCorner(img: ImgView2)
        roundedCorner(img: ImgView22)
        roundedCorner(img: ImgView3)
        roundedCorner(img: ImgView33)
        
        
        self.btnAddGrades.addTarget(self, action: #selector(addGradesClickMethod), for: .touchUpInside)
    }
    
    func roundedCorner(img : UIImageView){
        img.layer.cornerRadius = 10.0
        img.layer.borderWidth = 1
        img.layer.borderColor = UIColor.gray.cgColor
        img.layer.masksToBounds = true
    }
        
    @objc func addGradesClickMethod() {
        
        let dummyList = ["Apple", "Orange", "Banana", "Mango"]
        
        RPicker.selectOption(title: "Select Grade Level", cancelText: "Cancel", dataArray: dummyList, selectedIndex: 2) {[weak self] (selctedText, atIndex) in
            // TODO: Your implementation for selection
            // self?.outputLabel.text = selctedText + " selcted at \(atIndex)"
            
            self?.txtLevelName.text = String(selctedText)
        }
        
    }
    
    @IBAction func btn_box(sender: UIButton) {
        if (btnCheckBox.isSelected == true) {
            btnCheckBox.isSelected = false
            btnCheckBox.setImage(#imageLiteral(resourceName: "uncheckB"), for: .normal)
        }
        else {
            btnCheckBox.setImage(#imageLiteral(resourceName: "checkB"), for: .normal)
            btnCheckBox.isSelected = true
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TermsConditionAcceptVC") as? TermsConditionAcceptVC
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }

    //==========GENDER==========//
    @IBAction func btnMale_box(sender: UIButton) {
        if (btnMale.isSelected == true) {
            strGender = "Female"
            btnMale.setImage(UIImage(named: "uncheck"), for: .normal)
            btnFemale.setImage(UIImage(named: "checked"), for: .normal)
            btnMale.isSelected = false
            btnFemale.isSelected = true
        }
        else {
            strGender = "Male"
            btnMale.setImage(UIImage(named: "checked"), for: .normal)
            btnFemale.setImage(UIImage(named: "uncheck"), for: .normal)
            btnMale.isSelected = true
            btnFemale.isSelected = false
        }
    }
    
    @IBAction func btnFemale_box(sender: UIButton) {
        if (btnFemale.isSelected == true) {
            strGender = "Male"
            btnMale.setImage(UIImage(named: "checked"), for: .normal)
            btnFemale.setImage(UIImage(named: "uncheck"), for: .normal)
            btnFemale.isSelected = false
            btnMale.isSelected = true
        }
        else {
            strGender = "Female"
            btnFemale.setImage(UIImage(named: "checked"), for: .normal)
            btnMale.setImage(UIImage(named: "uncheck"), for: .normal)
            btnFemale.isSelected = true
            btnMale.isSelected = false
        }
    }
    
    //=================Address=============//
    
    @IBAction func btnHome_box(sender: UIButton) {
        if (btnHouse.isSelected == true) {
            btnHouse.setImage(UIImage(named: "uncheck"), for: .normal)
            btnApartment.setImage(UIImage(named: "checked"), for: .normal)
            btnOthers.setImage(UIImage(named: "checked"), for: .normal)
            
            btnHouse.isSelected = false
            btnApartment.isSelected = true
            btnOthers.isSelected = true
        }
        else {
            btnHouse.setImage(UIImage(named: "checked"), for: .normal)
            btnApartment.setImage(UIImage(named: "uncheck"), for: .normal)
            btnOthers.setImage(UIImage(named: "uncheck"), for: .normal)
            
            btnHouse.isSelected = true
            btnApartment.isSelected = false
            btnOthers.isSelected = false
        }
    }
    
    @IBAction func btnAppartment_box(sender: UIButton) {
        if (btnApartment.isSelected == true) {
            btnApartment.setImage(UIImage(named: "uncheck"), for: .normal)
            btnHouse.setImage(UIImage(named: "checked"), for: .normal)
            btnOthers.setImage(UIImage(named: "checked"), for: .normal)
            
            btnApartment.isSelected = false
            btnHouse.isSelected = true
            btnOthers.isSelected = true
        }
        else {
            btnApartment.setImage(UIImage(named: "checked"), for: .normal)
            btnHouse.setImage(UIImage(named: "uncheck"), for: .normal)
            btnOthers.setImage(UIImage(named: "uncheck"), for: .normal)
            
            btnApartment.isSelected = true
            btnHouse.isSelected = false
            btnOthers.isSelected = false
        }
    }
    
    @IBAction func btnOther_box(sender: UIButton) {
        if (btnOthers.isSelected == true) {
            btnOthers.setImage(UIImage(named: "uncheck"), for: .normal)
            btnHouse.setImage(UIImage(named: "checked"), for: .normal)
            btnApartment.setImage(UIImage(named: "checked"), for: .normal)
            
            btnOthers.isSelected = false
            btnHouse.isSelected = true
            btnApartment.isSelected = true
        }
        else {
            btnOthers.setImage(UIImage(named: "checked"), for: .normal)
            btnHouse.setImage(UIImage(named: "uncheck"), for: .normal)
            btnApartment.setImage(UIImage(named: "uncheck"), for: .normal)
            
            btnOthers.isSelected = true
            btnHouse.isSelected = false
            btnApartment.isSelected = false
        }
    }
    
    @IBAction func btnQuestionMarkForInfoText(sender: UIButton) {
        let alert = UIAlertController(title: String("Alert"), message: InformationTxt, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }


    //================================//
    @IBAction func btnDOBClickME(_ sender: Any) {
        self.displayAlertControllerDatePicker(title: "Alert", message: "Choose DOB")
    }
    
    @IBAction func btnRegistrationClickMe(_ sender : UIButton) {
        if self.txtName.text == "" {
            let alert = UIAlertController(title: String("Alert"), message: String("Name should not be empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        } else if self.txtEmail.text == "" {
            let alert = UIAlertController(title: String("Alert"), message: String("Email should not be empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        } else if !isValidEmail(testStr: self.txtEmail.text!) {
            print("Validate EmailID")
            let alert = UIAlertController(title: "Error!", message: "Please Enter a valid E-Mail Address.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
//        else if self.txtContactNumber.text == "" {
//            let alert = UIAlertController(title: String("Alert"), message: String("Contact Number should not be empty."), preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
//            }))
//            self.present(alert, animated: true, completion: nil)
//        }
        else if self.txtPassword.text == "" {
            let alert = UIAlertController(title: String("Alert"), message: String("Password should not be empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        }
//        else if self.txtDOB.text == "" {
//            let alert = UIAlertController(title: String("Alert"), message: String("DOB should not be empty."), preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
//            }))
//            self.present(alert, animated: true, completion: nil)
//        }
//        else if self.txtAddress.text == "" {
//            let alert = UIAlertController(title: String("Alert"), message: String("Address should not be empty."), preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
//            }))
//            self.present(alert, animated: true, completion: nil)
//        }
        
//        else if self.imageData == nil {
//            let alert = UIAlertController(title: String("Alert"), message: String("Upload birth certificate."), preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
//            }))
//            self.present(alert, animated: true, completion: nil)
//        }
//        else if self.imageData1 == nil  {
//            let alert = UIAlertController(title: String("Alert"), message: String("Upload id proof."), preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
//            }))
//            self.present(alert, animated: true, completion: nil)
//        }
//        else if self.imageData2 == nil  {
//            let alert = UIAlertController(title: String("Alert"), message: String("Upload residency proof."), preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
//            }))
//            self.present(alert, animated: true, completion: nil)
//        }
//        else if self.imageData3 == nil  {
//            let alert = UIAlertController(title: String("Alert"), message: String("Upload report card."), preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
//            }))
//            self.present(alert, animated: true, completion: nil)
//        }
        else {
           if btnCheckBox.isSelected == false{
                let alert = UIAlertController(title: "Alert", message: "Please accept terms and conditions", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
           }else{
                self.registrationAPI()
           }
        }
    }
    
    func registrationAPI() {
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            if(txtName.text!.isEqual("") ||  txtEmail.text!.isEqual("") || txtPassword.text!.isEqual("")) {
                let alertController = UIAlertController(title: "Alert", message: "Please fill the vacant field", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    print("you have pressed the Ok button");
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion:nil)
            }else{
                let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
                spinnerActivity.label.text = "Loading";
                spinnerActivity.detailsLabel.text = "Please Wait!!";
                self.view.isUserInteractionEnabled = false
                
                let parameters = ["action": "registration",
                                  "fullName": self.txtName.text ?? "",
                                  "email": self.txtEmail.text ?? "",
                                  "password": self.txtPassword.text ?? "",
                                  "contactNumber": self.txtContactNumber.text ?? "",
                                  "address": self.txtAddress.text ?? "",
                                  "dob": self.txtDOB.text ?? "",
                                  "gaurdianName": txtGaurdianName.text ?? "",
                                  "elementarySchool": txtSchoolName.text ?? "",
                                  "gradeLevel": txtLevelName.text ?? "",
                                  "gender": strGender,
                                  "device":"IOS",
                                  "deviceToken":UserDefaults.standard.string(forKey: "deviceFirebaseToken") ?? "APA91bFPtZDwmdAkbSUhcwqFTh2Y1tcmYFPcRFXQq0StFmkMIaM5V3_Ku8j"]as [String : Any]as [String : Any]
                
                var header = HTTPHeaders()
                header = ["Content-type": "multipart/form-data", "Content-Disposition" : "form-data"]
                AF.upload(multipartFormData: { multipartFormData in
                    for (key, value) in parameters {
                        if let temp = value as? String {
                            multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                        }
                        if let temp = value as? Int {
                            multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                        }
                        if let temp = value as? NSArray {
                            temp.forEach({ element in
                                let keyObj = key + "[]"
                                if let string = element as? String {
                                    multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                                } else
                                if let num = element as? Int {
                                    let value = "\(num)"
                                    multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                                }
                            })
                        }
                    }
                },
                to:BaseUrl, method: .post , headers: header)
                .responseJSON(completionHandler: { (response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.isUserInteractionEnabled = true;
                    if let err = response.error {
                        print(err)
                        return
                    }
                    print("Succesfully uploaded")
                    let jsonData = response.data
                    self.dismiss(animated:true, completion: nil)
                    if (jsonData != nil) {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            self.view.isUserInteractionEnabled = true
                        }
                        do {
                            if let json = try JSONSerialization.jsonObject(with: jsonData ?? Data(), options: []) as? [String: Any] {
                                    print(json)
                                let strStatus = json["status"]as? String ?? ""
                                if strStatus != "Fails"  {
                                    self.dataDictionary = json["data"] as? [String:Any] ?? [:]
                                    let dictLogin : NSDictionary? = self.dataDictionary as NSDictionary
                                    let prefs:UserDefaults = UserDefaults.standard
                                    prefs.set(self.dataDictionary["userId"] as? Int, forKey:"UserID")
                                    prefs.set(self.dataDictionary["fullName"] as? String, forKey:"Name")
                                    prefs.set(self.dataDictionary["email"] as? String, forKey:"Email")
                                    prefs.set(self.dataDictionary["address"] as? String, forKey:"Address")
                                    prefs.set(self.dataDictionary["contactNumber"] as? Int, forKey: "ContactNumber")
                                    prefs.set(dictLogin, forKey: "kAPI_LOGIN_DATA")
                                    prefs.synchronize()
                                    self.UpdateUserProfile(userid: self.dataDictionary["userId"] as? Int ?? 0)
                                }else{
                                    let alertController = UIAlertController(title: "Alert", message:json["msg"]as? String ?? "", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                        } catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                })
            }
        }
    }
    
    
    func UpdateUserProfile(userid: Int) {
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            self.view.isUserInteractionEnabled = false

            let parameters = ["action": "editprofile",
                              "fullName": self.txtName.text ?? "",
                              "contactNumber": self.txtContactNumber.text ?? "",
                              "address": (self.txtAddress.text ?? "").capitalized,
                              "userId": String(userid)]as [String : Any]as [String : Any]

            var header = HTTPHeaders()
            header = ["Content-type": "multipart/form-data", "Content-Disposition" : "form-data"]
            AF.upload(multipartFormData: { multipartFormData in
                for (key, value) in parameters {
                    if let temp = value as? String {
                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? Int {
                        multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key + "[]"
                            if let string = element as? String {
                                multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                            if let num = element as? Int {
                                let value = "\(num)"
                                multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
                if let img = self.imageData1 {
                    multipartFormData.append(img, withName:"Proof_of_Identity_back", fileName: "Proof_of_Identity_back.jpg", mimeType: "image/jpg")
                }
                if let img = self.imageData2 {
                    multipartFormData.append(img, withName:"Proof_of_Residency_back", fileName: "Proof_of_Residency_back.jpg", mimeType: "image/jpg")
                }
                if let img = self.imageData3 {
                    multipartFormData.append(img, withName:"Report_Card_back", fileName: "Report_Card_back.jpg", mimeType: "image/jpg")
                }
                //=========New=====//
                if let img = self.imageData11 {
                    multipartFormData.append(img, withName:"Proof_of_Identity", fileName: "Proof_of_Identity.jpg", mimeType: "image/jpg")
                }
                if let img = self.imageData22 {
                    multipartFormData.append(img, withName:"Proof_of_Residency", fileName: "Proof_of_Residency.jpg", mimeType: "image/jpg")
                }
                if let img = self.imageData33 {
                    multipartFormData.append(img, withName:"Report_Card", fileName: "Report_Card.jpg", mimeType: "image/jpg")
                }
            },
            to:BaseUrl, method: .post , headers: header)
            .responseJSON(completionHandler: { (response) in
                print(response)
                MBProgressHUD.hide(for: self.view, animated: true)
                self.view.isUserInteractionEnabled = true;
                if let err = response.error {
                    print(err)
                    return
                }
                print("Succesfully uploaded")
                let jsonData = response.data
                self.dismiss(animated:true, completion: nil)
                if (jsonData != nil) {
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.view.isUserInteractionEnabled = true
                    }
                    do {
                        if let json = try JSONSerialization.jsonObject(with: jsonData ?? Data(), options: []) as? [String: Any] {
                            print(json)
                            let strStatus = json["status"]as? String ?? ""
                            
                            let alertController = UIAlertController(title: "Alert", message:json["msg"]as? String ?? "", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                if strStatus != "Fails"  { // msg //
                                    self.dataDictionary = json["data"] as? [String:Any] ?? [:]
                                    let dictLogin : NSDictionary? = self.dataDictionary as NSDictionary
                                    let prefs:UserDefaults = UserDefaults.standard
                                    prefs.set(self.dataDictionary["userId"] as? Int, forKey:"UserID")
                                    prefs.set(self.dataDictionary["fullName"] as? String, forKey:"Name")
                                    prefs.set(self.dataDictionary["email"] as? String, forKey:"Email")
                                    prefs.set(self.dataDictionary["address"] as? String, forKey:"Address")
                                    prefs.set(self.dataDictionary["contactNumber"] as? Int, forKey: "ContactNumber")
                                    prefs.set(dictLogin, forKey: "kAPI_LOGIN_DATA")
                                    prefs.synchronize()
                                    
                                    UserDefaults.standard.setValue("1", forKey: "profileUpdate")
                                    UserDefaults.standard.setValue("1", forKey: "loginStatus")
                                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardVC") as? DashboardVC
                                    self.navigationController?.pushViewController(vc!, animated: false)
                                }
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    } catch let error as NSError {
                        print("Failed to load: \(error.localizedDescription)")
                    }
                }
            })
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    // MARK :- button Action
    @IBAction func btnBackClickMe(_ sender : UIButton){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnLoginClickMe(_ sender : UIButton){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    
    // MARK :- Get Image from Gallary
//    @IBAction func btnDocumentClickMe(_ sender : UIButton){
//        UploadDocTag = 1
//        self.picImageFromGalleyOrCamer()
//    }
    @IBAction func btnIDProofClickMe(_ sender : UIButton){
        imgSetTag = 1
        UploadIdProofTag = 2
        self.picImageFromGalleyOrCamer()
    }
    @IBAction func btnResidencyClickMe(_ sender : UIButton){
        imgSetTag = 2
        UploadResidencyTag = 3
        self.picImageFromGalleyOrCamer()
    }
    @IBAction func btnReportClickMe(_ sender : UIButton){
        imgSetTag = 3
        UploadReportTag = 4
        self.picImageFromGalleyOrCamer()
    }
   //================
    @IBAction func btnIDProofFrontClickMe(_ sender : UIButton){
        imgSetTag = 11
        UploadIdProofTagFront = 22
        self.picImageFromGalleyOrCamer()
    }
    @IBAction func btnResidencyFrontClickMe(_ sender : UIButton){
        imgSetTag = 22
        UploadResidencyTagFront = 33
        self.picImageFromGalleyOrCamer()
    }
    @IBAction func btnReportFrontClickMe(_ sender : UIButton){
        imgSetTag = 33
        UploadReportTagFront = 44
        self.picImageFromGalleyOrCamer()
    }
    
    func picImageFromGalleyOrCamer(){
        let actionSheetController: UIAlertController = UIAlertController(title: "Action Sheet", message: "Choose an option!", preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelAction)
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                self.tagCamera_Gallery = 1
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(takePictureAction)
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Camera Roll", style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                self.tagCamera_Gallery = 0
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(choosePictureAction)
        if let popoverController = actionSheetController.popoverPresentationController {
            popoverController.sourceView = self.ImgView
        }
        self.present(actionSheetController, animated: true, completion: nil)
    }

    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width:size.width * heightRatio, height:size.height * heightRatio)
        } else {
            newSize = CGSize(width:size.width * widthRatio,  height:size.height * widthRatio)
        }
        let rect = CGRect(x:0, y:0, width:newSize.width, height:newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 8.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        photo1 = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage //2
        let img = self.resizeImage(image: self.photo1!, targetSize:CGSize(width:100.0 , height:100.0))
        
        if imgSetTag == 1{
            ImgView.image = img
        }
        if imgSetTag == 2{
            ImgView2.image = img
        }
        if imgSetTag == 3{
            ImgView3.image = img
        }
        if imgSetTag == 11{
            ImgView1.image = img
            if let image  = ImgView1.image,let imageData = image.jpegData(compressionQuality: 8.0) {
                self.imageData11 = imageData
            }
        }
        if imgSetTag == 22{
            ImgView22.image = img
        }
        if imgSetTag == 33{
            ImgView33.image = img
            if let image  = ImgView33.image,let imageData = image.jpegData(compressionQuality: 8.0) {
                self.imageData33 = imageData
            }
        }
        
        // =========1=========== //
        if(UploadIdProofTag == 2) {
            if let image  = ImgView.image,let imageData = image.jpegData(compressionQuality: 8.0) {
                self.imageData1 = imageData
            }
        }
    // =========2========== //
        if(UploadResidencyTag == 3) {
            if let image  = ImgView2.image,let imageData = image.jpegData(compressionQuality: 8.0) {
               self.imageData2 = imageData
            }
        }
        if(UploadResidencyTagFront == 33) {
            if let image  = ImgView22.image,let imageData = image.jpegData(compressionQuality: 8.0) {
                self.imageData22 = imageData
            }
        }
   // =========3======== //
        if(UploadReportTag == 4) {
            if let image  = ImgView3.image,let imageData = image.jpegData(compressionQuality: 8.0) {
                self.imageData3 = imageData
            }
        }
        self.dismiss(animated:true, completion: nil)
    }
}


/** --------------------------------------------------------
*        Date Picker View defined by Targets.
*  --------------------------------------------------------
*/
extension RegisterVC {
    func displayAlertControllerDatePicker(title:String, message:String) {
        datePicker  = UIDatePicker(frame: CGRect(x: 10, y: 65, width: self.view.frame.width - 20, height: 150))
        datePicker.addTarget(self, action: #selector(RegisterVC.dueDateChanged(_:)), for: UIControl.Event.valueChanged)
        datePicker.datePickerMode = UIDatePicker.Mode.date
        let editRadiusAlert = UIAlertController(title:title , message: message, preferredStyle: UIAlertController.Style.actionSheet)
        let height:NSLayoutConstraint = NSLayoutConstraint(item: editRadiusAlert.view as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 330)
        editRadiusAlert.view.addConstraint(height);
        editRadiusAlert.view.addSubview(datePicker)
        
        editRadiusAlert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
        editRadiusAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: cancelClickEvent))
        self.present(editRadiusAlert, animated: true)

        datePicker.datePickerMode = UIDatePicker.Mode.date
        datePicker.minimumDate = Calendar.current.date(byAdding: .month, value: -1116, to: Date()) // 90*12+36 = 1116  (90 yr)
        datePicker.maximumDate = Calendar.current.date(byAdding: .month, value: -36, to: Date()) // 3*12 = 36 (3 yr)
    }
    
    @IBAction func dueDateChanged(_ sender: Any) {
        self.txtName.resignFirstResponder()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strDate = dateFormatter.string(from: (sender as AnyObject).date)
        txtDOB.text = strDate
        print(txtDOB.text as Any)
    }
    func cancelClickEvent(action: UIAlertAction) {
    }
}
