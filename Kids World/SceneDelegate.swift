//
//  SceneDelegate.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    func sceneDidDisconnect(_ scene: UIScene) { }
    func sceneDidBecomeActive(_ scene: UIScene) { }
    func sceneWillResignActive(_ scene: UIScene) { }
    func sceneWillEnterForeground(_ scene: UIScene) { }
    func sceneDidEnterBackground(_ scene: UIScene) { }
}

/*
 // MARK :- NOTATION
 
 @IBOutlet weak var viewFullNavConstraint : NSLayoutConstraint!
 // self.viewFullNavConstraint.constant = 64
 
 @IBAction func btnSignInClickMe(_ sender : UIButton){
     let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
     self.navigationController?.pushViewController(vc!, animated: false)
 }

 */
