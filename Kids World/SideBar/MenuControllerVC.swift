//
//  MenuControllerVC.swift
//  SidebarMenu
//
//  Created by Apple  on 16/10/19.
//  Copyright © 2019 AppCoda. All rights reserved.
//

import UIKit
import Alamofire

class MenuControllerVC: UIViewController {

    let cellReuseIdentifier = "menuControllerVCTableCell"
    
    var bgImage: UIImageView?
    
    var roleIs:String!
    
    var responseDictionary:[String:Any] = [:]
    var message:String?
    var dataDictionary:[String:Any] = [:]
    var status:String?
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = .systemYellow
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "NAVIGATION"
            lblNavigationTitle.textColor = .white
        }
    }
    
    // driver
    var arrMenuItemList = ["Dashboard","Edit Profile","Friends","Daily Affirmations","Affirmations","Messages","Reminders","Friends request status","Users","Change Password","Notes","King James Bible","About Boo Check","Privacy Policy","Term & Conditions","Help","Sign Out"]
    
    
    var arrMenuItemListImage = ["home","editL","chatL","article","noteQL","noteQL","goalL","groupL","groupL","lcokL","tripL","info","info","info","info","info","logout"]
    
    
   
    
    @IBOutlet var menuButton:UIButton!
    
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            tbleView.delegate = self
            tbleView.dataSource = self
            // tbleView.tableFooterView = UIView.init()
            tbleView.backgroundColor = .white
            // tbleView.separatorInset = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
            // tbleView.separatorColor = .white
        }
    }
    @IBOutlet weak var lblMainTitle:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    
    var dict : [String : Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideBarMenuClick()
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.dict = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            print(person as Any)
            
            /*
             ["userId": 99, "ssnImage": , "socialId": , "image": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1587560500Corona.png, "zipCode": , "device": , "dob": , "email": restaurent@gmail.com, "AutoInsurance": , "RoutingNo": vstvush6sg6, "AccountHolderName": jjsjs, "accountType": Saving, "country": India, "deviceToken": , "logitude": 78.1694957, "middleName": , "fullName": restaurent, "AccountNo": 8505858545884555, "longitude": 78.1694957, "foodTag": Veg, "wallet": 0, "role": Restaurant, "contactNumber": 9494645544, "firebaseId": , "address": Gwalior, Madhya Pradesh 474008, India, "gender": , "drivlingImage": , "socialType": , "state": , "BankName": djdgsgs, "lastName": , "latitude": 26.2313245, "companyBackground": http://demo2.evirtualservices.com/food-delivery/site/img/uploads/users/1587560500Corona.png]
             (lldb)
             */
            // roleIs
            
            self.lblMainTitle.text = (person["fullName"] as! String)
            self.lblAddress.text = (person["address"] as! String)
            
            
          
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @objc func sideBarMenuClick() {
        if revealViewController() != nil {
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
}

extension MenuControllerVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return arrMenuItemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MenuControllerVCTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MenuControllerVCTableCell
        
        cell.backgroundColor = .clear
      
        cell.lblName.text = arrMenuItemList[indexPath.row]
        cell.imgProfile.image = UIImage(named: arrMenuItemListImage[indexPath.row])
        cell.imgProfile.backgroundColor = .clear
        
        cell.lblName.textColor = .white
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 0 {

            let myString = "foodOrderEarningCheck"
            UserDefaults.standard.set(myString, forKey: "keyFoodOrderEarnings")
                
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
            self.view.window?.rootViewController = sw
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "RTotalEarningsId")
            let navigationController = UINavigationController(rootViewController: destinationController!)
            sw.setFront(navigationController, animated: true)
            
            } else if indexPath.row == 12 { // logout
                let alertController = UIAlertController(title: "Logout!", message: "Are you sure you want to logout ?", preferredStyle: .actionSheet)
                
                let okAction = UIAlertAction(title: "Yes, Logout", style: .default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                    
                    self.logoutWB()
                    
                    
                }
                alertController.addAction(okAction)
                
                let dismissAction = UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                    NSLog("OK Pressed")
                    
                }
                alertController.addAction(dismissAction)
                self.present(alertController, animated: true, completion: nil)
            }
        
        
    }
    
    @objc func logoutWB() {
        /*ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "logging out...")
    
        let urlString = BASE_URL_EXPRESS_PLUS
  
        var parameters:Dictionary<AnyHashable, Any>!
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
           
          
            parameters = [
                "action"             : "logout",
                "userId"             : person["userId"] as Any, // done
                              
            ]
         }
                   print("parameters-------\(String(describing: parameters))")
                   
                   Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON
                       {
                           response in
               
                           switch(response.result) {
                           case .success(_):
                              if let data = response.result.value {

                               let JSON = data as! NSDictionary
                                print(JSON as Any)
                               
                               var strSuccess : String!
                               strSuccess = JSON["status"]as Any as? String
                               
                                 // var strSuccessAlert : String!
                                 // strSuccessAlert = JSON["msg"]as Any as? String
                               
                               if strSuccess == String("success") {
                                print("yes")
                                 
                                 ERProgressHud.sharedInstance.hide()
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue("", forKey: "keyLoginFullData")
                                defaults.setValue(nil, forKey: "keyLoginFullData")

                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let sw = storyboard.instantiateViewController(withIdentifier: "sw") as! SWRevealViewController
                                self.view.window?.rootViewController = sw
                                let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeId")
                                let navigationController = UINavigationController(rootViewController: destinationController!)
                                sw.setFront(navigationController, animated: true)
                                
                                  // let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WelcomeId") as? Welcome
                                  // self.navigationController?.pushViewController(push!, animated: true)
                                 
                               }
                               else {
                                print("no")
                                 ERProgressHud.sharedInstance.hide()
                               }
                           }

                           case .failure(_):
                               print("Error message:\(String(describing: response.result.error))")
                               
                               ERProgressHud.sharedInstance.hide()
                               
                               let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                               
                               let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                       UIAlertAction in
                                       NSLog("OK Pressed")
                                   }
                               
                               alertController.addAction(okAction)
                               
                               self.present(alertController, animated: true, completion: nil)
                               
                               break
                            }
                       }*/
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension MenuControllerVC: UITableViewDelegate {
    
}
