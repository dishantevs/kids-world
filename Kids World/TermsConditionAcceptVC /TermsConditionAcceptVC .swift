//
//  HelpVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import MBProgressHUD

class TermsConditionAcceptVC : UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var txtView: UITextView!

    // slider//
    var isOpen = false
    var screenCheck = false

    var responseDictionary:[String:Any] = [:]
    var dataDictionary:[String:Any] = [:]
    var status:String?
    var msg:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        btnMenu.centerTextAndImage(spacing: 10.0)
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
        // slider //
        self.btnMenu.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        getHelp()
    }
    
    // MARK :- button Action
    @IBAction func sideBarMenuClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
//    @IBAction func btnAccpeetClickMe(_ sender : UIButton){
//    }

        
    func getHelp() {
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let values = ["action":"termAndConditions"] as [String : Any]
                print("Values \(values)")
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                AF.request(request)
                    .responseJSON { response in
                        switch response.result {
                        case .failure(let error):
                            // SVProgressHUD.dismiss()
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                                let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            DispatchQueue.main.async {
                                print("This is run on the main queue, after the previous code in outer block")
                            }
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                self.responseDictionary = dict
                            }
                            
                            self.status = self.responseDictionary["status"] as? String
                            if self.status == "Fails"
                            {
                                let alertController = UIAlertController(title: "Alert", message:self.responseDictionary["msg"] as? String, preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    print("you have pressed the Ok button");
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                            else
                            {
                                let str = self.responseDictionary["msg"] as? String ?? ""
                                let data = Data(str.utf8)
                                if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) {
                                    self.txtView.attributedText = attributedString
                                }
                            }
                        }
                    }
            }
        }
    }
}
