//
//  TimelineVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD
import GrowingTextView
import SDWebImage

import AVKit
import AVFoundation

class TimelineVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var what_user_select_option:String! = "0"
    
    // image
    var imageStr:String!
    var imgData:Data!

    // video
    var videoData: NSData!
    var videoURL : NSURL!
    
    var save_video_url : URL!
    
    @IBOutlet weak var viewUploadTestVIEW:UIView! {
        didSet {
            viewUploadTestVIEW.isHidden = true
        }
    }
    @IBOutlet weak var img_check:UIImageView! {
        didSet {
            img_check.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
            img_check.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            img_check.layer.shadowOpacity = 1.0
            img_check.layer.shadowRadius = 15.0
            img_check.layer.masksToBounds = false
            img_check.layer.cornerRadius = 15
            img_check.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnMenu: UIButton!
    
    @IBOutlet weak var tbl: UITableView! {
        didSet {
            tbl.backgroundColor = .white
        }
    }
    
    var responseDictionary:[String:Any] = [:]
    var responseArray:[[String:Any]] = [[:]]
    
    var arrListOfTimelineData : NSMutableArray! = []
    
    // slider//
    var isOpen = false
    var screenCheck = false
    
    @IBOutlet weak var inputToolbar: UIView!
    @IBOutlet weak var textView: GrowingTextView!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ImgView: UIImageView!
    @IBOutlet weak var imgHeightConstraint : NSLayoutConstraint!
    
    var imagePicker :UIImagePickerController?
    var photo1:UIImage?
    var imageData:Data?
    
    @IBOutlet weak var btnSendMessage:UIButton! {
        didSet {
            btnSendMessage.tintColor = .white
        }
    }
    
    @IBOutlet weak var imgPlay:UIImageView! {
        didSet {
            imgPlay.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
            imgPlay.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            imgPlay.layer.shadowOpacity = 1.0
            imgPlay.layer.shadowRadius = 15.0
            imgPlay.layer.masksToBounds = false
            imgPlay.layer.cornerRadius = 15
            imgPlay.backgroundColor = .clear
            imgPlay.isHidden = true
        }
    }
    
    @IBOutlet weak var btnAttachment:UIButton! {
        didSet {
            btnAttachment.tintColor = .white
        }
    }
    
    @IBOutlet weak var indicators:UIActivityIndicatorView! {
        didSet {
            indicators.color = .white
        }
    }
    
    @IBOutlet weak var lblProcessingImage:UILabel! {
        didSet {
            lblProcessingImage.textColor = .white
            lblProcessingImage.text = "processing..."
        }
    }
    
    @IBOutlet weak var uploadingImageView:UIView! {
        didSet {
          //  uploadingImageView.backgroundColor = APP_BASIC_COLOR
            uploadingImageView.isHidden = true
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear // UIColor.init(red: 244.0/255.0, green: 246.0/255.0, blue: 248.0/255.0, alpha: 1)
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    @IBOutlet weak var navigationBar:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // btnMenu.centerTextAndImage(spacing: 10.0)
        // self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
        // slider //
        // self.btnMenu.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        let directions: [UISwipeGestureRecognizer.Direction] = [.left,.right,.up,.down]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(DashboardVC.handleSwipe1(gesture:)))
            gesture.direction = direction
            self.view?.addGestureRecognizer(gesture)
        }
        
        self.btnAttachment.addTarget(self, action: #selector(attachmentClickMethod), for: .touchUpInside)
        
        //
        self.btnSendMessage.addTarget(self, action: #selector(submit_post_with_image_wb), for: .touchUpInside)
        
        // *** Listen to keyboard show / hide ***
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        // *** Hide keyboard when tapping outside ***
        
        textView.layer.cornerRadius = 4.0
        // self.imgHeightConstraint.constant = 0
        self.tbl.isHidden = true
        self.callTimeLineListAPI()
    }
    
    @objc func attachmentClickMethod() {
        
        let alertController = UIAlertController(title: "Upload", message: nil, preferredStyle: .actionSheet)
        
        let okAction = UIAlertAction(title: "Upload photo via Camera", style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")
            
            self.openCamera1()
            
        }
        
        alertController.addAction(okAction)
        
        // image gallery
        let okAction2 = UIAlertAction(title: "Upload photo via Gallery", style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")
            
            self.openGallery1()
            
        }
        
        alertController.addAction(okAction2)
        
        // video via camera
        let okAction3 = UIAlertAction(title: "Upload video via Camera", style: .default) {
                UIAlertAction in
                NSLog("OK Pressed")
            
            self.what_user_select_option = "3"
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
            
        }
        
        alertController.addAction(okAction3)
        
        // video via gallery
        let okAction4 = UIAlertAction(title: "Upload video via Gallery", style: .default) {
            UIAlertAction in
            NSLog("OK Pressed")
            
            self.what_user_select_option = "4"
            var picker = UIImagePickerController()
            picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .savedPhotosAlbum
            picker.mediaTypes = ["public.movie"]
            picker.allowsEditing = false
            self.present(picker, animated: true, completion: nil)
            
        }
        
        alertController.addAction(okAction4)
        
        let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel) {
                UIAlertAction in
            NSLog("OK Pressed")
            
        }
        
        alertController.addAction(dismissAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @objc func openCamera1() {
        self.what_user_select_option = "1"
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    @objc func openGallery1() {
        self.what_user_select_option = "2"
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        /*photo1 = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as? UIImage //2
        let img = self.resizeImage(image: self.photo1!, targetSize:CGSize(width:100.0 , height:100.0))
        ImgView.contentMode = .scaleAspectFill //3
        ImgView.image = img //4
        if let image  = ImgView.image,let imageData = image.jpegData(compressionQuality: 8.0) {
            self.imageData = imageData
        }
        self.imgHeightConstraint.constant = 180
        self.dismiss(animated:true, completion: nil)*/
        
        if self.what_user_select_option == "1" || self.what_user_select_option == "2" {
            self.viewUploadTestVIEW.isHidden = false
            
            self.imgPlay.isHidden = true
            
            let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            self.img_check.image = image_data // show image on image view
            let imageData:Data = image_data!.pngData()!
            self.imageStr = imageData.base64EncodedString()
            self.dismiss(animated: true, completion: nil)
           
            self.imgData = image_data!.jpegData(compressionQuality: 0.2)!
            self.imageData = image_data!.jpegData(compressionQuality: 0.2)!
            
            self.dismiss(animated: true, completion: nil)
            
        } else {
            
            self.videoURL = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerMediaURL")] as? NSURL
            
            do {
                
                self.videoData = try NSData(contentsOf: videoURL! as URL, options: .mappedIfSafe)
                
                // UserDefaults.standard.set(videoData, forKey: "VIDEODATA")
                self.imgPlay.isHidden = false
                print(self.videoURL as Any)
                
                // let player = AVPlayer(url: videoURL as URL)
                
                self.viewUploadTestVIEW.isHidden = false
                
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TimelineVC.image_click_when_video_select(_:)))

                self.img_check.isUserInteractionEnabled = true
                self.img_check.addGestureRecognizer(tapGestureRecognizer)
                
                if let videoUrl = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.mediaURL.rawValue)] as? URL {
                    
                    // print(videoUrl as Any)
                    
                    // print(type(of: videoUrl))
                    
                    self.save_video_url = videoUrl
                }
                
                // self.uploadVideoOnServer()
                
            } catch {
                print(error)
                return
            }
            
            
            
            self.dismiss(animated: true, completion: nil)
            
        }
        
    }
    
    @objc func image_click_when_video_select(_ sender:AnyObject) {
        
        
        
        // let videoURL = URL(string: self.videoURL)
        let player = AVPlayer(url: videoURL as URL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        
    }
    
    /*@objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if self.what_user_select_option == "1" || self.what_user_select_option == "2" {
            
            let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            self.img_check.image = image_data // show image on image view
            let imageData:Data = image_data!.pngData()!
            self.imageStr = imageData.base64EncodedString()
            self.dismiss(animated: true, completion: nil)
           
            self.imgData = image_data!.jpegData(compressionQuality: 0.2)!
            
            self.dismiss(animated: true, completion: nil)
            
        } else {
            
            videoURL = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerMediaURL")] as? NSURL
            do {
                videoData = try NSData(contentsOf: videoURL! as URL, options: .mappedIfSafe)
                
                UserDefaults.standard.set(videoData, forKey: "VIDEODATA")

                print(videoURL as Any)
                
                // imgFeed.image = self.thumbnailForVideoAtURL(url: videoURL!)
                //                self.uploadVideoOnServer()
                
            } catch {
                print(error)
                return
            }
            
            
            
            self.dismiss(animated: true, completion: nil)
            
        }
        
    }*/
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var keyboardHeight = UIScreen.main.bounds.height - endFrame.origin.y
            if #available(iOS 11, *) {
                if keyboardHeight > 0 {
                    keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                }
            }
            textViewBottomConstraint.constant = keyboardHeight + 8
            view.layoutIfNeeded()
        }
    }

    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    
    
    func callTimeLineListAPI() {
        
        if isInternetAvailable() == false {
            
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        else {
            
            // let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            // spinnerActivity.label.text = "Loading";
            // spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                let values = ["action":"articlelist"] as [String : Any]as [String : Any]
                print("Values \(values)")
                
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                
                AF.request(request)
                    .responseJSON { response in
                        MBProgressHUD.hide(for:self.view, animated: true)
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            // self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                
                                print(dict as Any)
                                // arrListOfTimelineData
                                
                                self.uploadingImageView.isHidden = true
                                
                                self.arrListOfTimelineData.removeAllObjects()
                                
                                var ar : NSArray!
                                ar = (dict["data"] as! Array<Any>) as NSArray
                                self.arrListOfTimelineData.addObjects(from: ar as! [Any])
                                
                                
                                // self.responseArray = dict["data"] as? [[String : Any]] ?? [[:]]
                                // print(self.responseArray)
                                if(self.arrListOfTimelineData.count != 0){
                                    self.tbl.isHidden = false
                                    
                                    self.tbl.delegate = self
                                    self.tbl.dataSource = self
                                    self.tbl.reloadData()
                                    
                                } else {
                                    self.tbl.isHidden = true
                                }
                                
                            }
                        }
                    }
            }
        }
    }
    
    //MARK: Table and Collection View
    @objc func tapLikeCheckHandle(_ sender : UIButton) { // videoId , Video
        let dict  = self.arrListOfTimelineData[sender.tag] as? [String :Any]
        self.callCommonAPI(strAction: "likes", postId: dict!["communtyId"]as?Int ?? 0)
    }
    
    @objc func tapCommentCheckHandle(_ sender : UIButton) { // videoId , Video
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CommentsVC") as? CommentsVC
        vc?.DictGetValue = (self.arrListOfTimelineData[sender.tag] as? [String :Any])!
        self.navigationController?.pushViewController(vc!, animated: false)
        
    }
    
    @objc func tapShareCheckHandle(_ sender : UIButton) {
        
        let dict = self.arrListOfTimelineData[sender.tag] as? [String :Any]
        
        let textToShare = [dict!["name"]as?String ?? "", dict!["image"]as?String ?? ""] as [Any]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @objc func tapMoreCheckHandle(_ sender : UIButton) {
        
        let dict : [String :Any] = self.responseArray[sender.tag]
        let actionSheetController: UIAlertController = UIAlertController(title: "Action Sheet", message: "Choose an option!", preferredStyle: .actionSheet)
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            
        }
        
        actionSheetController.addAction(cancelAction)
        
        let takePictureAction: UIAlertAction = UIAlertAction(title: "About User", style: .default) { action -> Void in
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UserProfileDetailsVC") as? UserProfileDetailsVC
            vc?.communityId = String(dict["communtyId"]as?Int ?? 0)
            self.navigationController?.pushViewController(vc!, animated: false)
        }
        
        actionSheetController.addAction(takePictureAction)
        
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Post Report", style: .default) { action -> Void in
            self.callCommonAPI(strAction: "report", postId: dict["communtyId"]as?Int ?? 0)}
        actionSheetController.addAction(choosePictureAction)
        self.present(actionSheetController, animated: true, completion: nil)
        
    }
    
    func callCommonAPI(strAction: String , postId : Int) {
        
        if isInternetAvailable() == false {
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        
        else {
            
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                
                let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
                
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                let values = ["action":strAction,
                              "postId":postId,
                              "userId":String(dict["userId"]as? Int ?? 0)] as [String : Any]
                
                print("Values \(values)")
                
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                
                AF.request(request)
                    .responseJSON { response in
                        MBProgressHUD.hide(for:self.view, animated: true)
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            // self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                
                                 print(dict as Any)
                                self.callTimeLineListAPI()
                                
                                /*let alertController = UIAlertController(title: "Alert", message:dict["msg"] as? String ?? "" , preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    if(dict["status"] as? String ?? "" == "Success"){
                                        if(strAction == "likes"){
                                            self.callTimeLineListAPI()
                                        }
                                    }
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion:nil)*/
                                
                                
                                
                            }
                        }
                    }
            }
        }
    }
    
    @IBAction func btnPickImage(_ sender : UIButton) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Action Sheet", message: "Choose an option!", preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelAction)
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(takePictureAction)
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Choose From Camera Roll", style: .default) { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(choosePictureAction)
        if let popoverController = actionSheetController.popoverPresentationController {
            popoverController.sourceView = self.ImgView
        }
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width:size.width * heightRatio, height:size.height * heightRatio)
        } else {
            newSize = CGSize(width:size.width * widthRatio,  height:size.height * widthRatio)
        }
        let rect = CGRect(x:0, y:0, width:newSize.width, height:newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 8.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    
    
    @objc func submit_post_with_image_wb() {
        
        if self.textView.text == "" {
            
            let alert = UIAlertController(title: String("Alert"), message: String("Enter your posting words."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            if self.what_user_select_option == "1" || self.what_user_select_option == "2" {
                
                self.viewUploadTestVIEW.isHidden = true
                self.uploadingImageView.isHidden = false
                self.indicators.startAnimating()
                
                // self.imgHeightConstraint.constant = 0
                self.AddArticalAPI()
                self.textView.text = ""
                
            } else {
                
                self.viewUploadTestVIEW.isHidden = true
                self.uploadingImageView.isHidden = false
                
                self.add_article_with_video_wb()
                
                // self.imgHeightConstraint.constant = 0
                // self.AddArticalAPI()
                self.textView.text = ""
                
            }
            
        }
        
    }
    
    func AddArticalAPI() {
        
        if isInternetAvailable() == false {
            
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            self.view.isUserInteractionEnabled = false
            let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
            
            let parameters = ["action": "addarticle",
                              "userId": String(dict["userId"] as? Int ?? 0),
                              "name_en": self.textView.text ?? ""] as [String : Any]as [String : Any]
            
            var header = HTTPHeaders()
            
            header = ["Content-type": "multipart/form-data",
                      "Content-Disposition" : "form-data"]
            
            AF.upload(multipartFormData: { multipartFormData in
                for (key, value) in parameters {
                    if let temp = value as? String {
                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? Int {
                        multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key + "[]"
                            if let string = element as? String {
                                multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                            if let num = element as? Int {
                                let value = "\(num)"
                                multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
                
                if let img = self.imageData {
                    multipartFormData.append(img, withName:"image", fileName: "image.jpg", mimeType: "image/jpg")
                }
                
            },
                      to:BaseUrl, method: .post , headers: header)
                .responseJSON(completionHandler: { (response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.isUserInteractionEnabled = true;
                    if let err = response.error {
                        print(err)
                        return
                    }
                    print("Succesfully uploaded")
                    let jsonData = response.data
                    self.dismiss(animated:true, completion: nil)
                    if (jsonData != nil) {
                        DispatchQueue.main.async {
                            // MBProgressHUD.hide(for: self.view, animated: true)
                            self.view.isUserInteractionEnabled = true
                        }
                        do {
                            if let json = try JSONSerialization.jsonObject(with: jsonData ?? Data(), options: []) as? [String: Any] {
                                print(json)
                                let strStatus = json["status"]as? String ?? ""
                                
                                if strStatus != "Fails"  { // msg
                                    
                                    self.imageData = nil
                                    self.uploadingImageView.isHidden = true
                                    
                                    self.callTimeLineListAPI()
                                    
                                    /*let alertController = UIAlertController(title: "Alert", message:"Post Successfully", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                        self.callTimeLineListAPI()
                                    }
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion:nil)*/
                                    
                                } else {
                                    let alertController = UIAlertController(title: "Alert", message:"Unable To Post!", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                        } catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                })
        }
    }
    
    // MARK:- ADD ARTICLE WITH VIDEO -
    @objc func add_article_with_video_wb() {
        
        if isInternetAvailable() == false {
            
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            // let spinnerActivity = MBProgressHUD.showAdded(to: self.view, animated: true);
            // spinnerActivity.label.text = "Loading";
            // spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            self.indicators.startAnimating()
            
            self.view.isUserInteractionEnabled = false
            let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
            
            let parameters = ["action": "addarticle",
                              "userId": String(dict["userId"] as? Int ?? 0),
                              "name_en": self.textView.text ?? ""] as [String : Any]
            
            var header = HTTPHeaders()
            
            header = ["Content-type": "multipart/form-data",
                      "Content-Disposition" : "form-data"]
            
            AF.upload(multipartFormData: { multipartFormData in
                for (key, value) in parameters {
                    if let temp = value as? String {
                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? Int {
                        multipartFormData.append("\(temp)".data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key + "[]"
                            if let string = element as? String {
                                multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                            if let num = element as? Int {
                                let value = "\(num)"
                                multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
                
                // if let img = self.imageData {
                multipartFormData.append(self.save_video_url, withName:"video", fileName: "file.mp4", mimeType: "video/mp4")
                // }
                
            },
                      to:BaseUrl, method: .post , headers: header)
                .responseJSON(completionHandler: { (response) in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.view.isUserInteractionEnabled = true;
                    if let err = response.error {
                        print(err)
                        return
                    }
                    print("Succesfully uploaded")
                    let jsonData = response.data
                    self.dismiss(animated:true, completion: nil)
                    if (jsonData != nil) {
                        DispatchQueue.main.async {
                            // MBProgressHUD.hide(for: self.view, animated: true)
                            self.view.isUserInteractionEnabled = true
                        }
                        do {
                            if let json = try JSONSerialization.jsonObject(with: jsonData ?? Data(), options: []) as? [String: Any] {
                                print(json)
                                let strStatus = json["status"]as? String ?? ""
                                
                                if strStatus != "Fails"  { // msg
                                    
                                    self.imageData = nil
                                    self.uploadingImageView.isHidden = true
                                    
                                    self.callTimeLineListAPI()
                                    
                                    /*let alertController = UIAlertController(title: "Alert", message:"Post Successfully", preferredStyle: .alert)
                                     let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                     self.callTimeLineListAPI()
                                     }
                                     alertController.addAction(okAction)
                                     self.present(alertController, animated: true, completion:nil)*/
                                    
                                } else {
                                    let alertController = UIAlertController(title: "Alert", message:"Unable To Post!", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                        } catch let error as NSError {
                            print("Failed to load: \(error.localizedDescription)")
                        }
                    }
                })
        }
    }
    
    // Left Slider //
    @objc func handleSwipe1(gesture: UISwipeGestureRecognizer) {
        print(gesture.direction)
        switch gesture.direction {
            
        case UISwipeGestureRecognizer.Direction.left:
            isOpen = false
            if screenCheck == true{
                let viewMenuBack : UIView = view.subviews.last!
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    var frameMenu : CGRect = viewMenuBack.frame
                    frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                    viewMenuBack.frame = frameMenu
                    viewMenuBack.layoutIfNeeded()
                    viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
                    
                })
                screenCheck = false
            }else{
            }
            print("left swipe")
        case UISwipeGestureRecognizer.Direction.right:
            print("right swipe")
            isOpen = true
            if screenCheck == false{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let menuVC  = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController
                self.view.addSubview(menuVC!.view)
                self.addChild(menuVC!)
                menuVC!.view.layoutIfNeeded()
                
                menuVC!.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
                
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    menuVC!.view.frame=CGRect(x: 0, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
                }, completion:nil)
                screenCheck = true
            }else{
            }
        default:
            print("other swipe")
        }
    }
    
    @objc func sideBarMenuClick() {
        if(!isOpen){
            screenCheck = true
            isOpen = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController
            self.view.addSubview(menuVC!.view)
            self.addChild(menuVC!)
            menuVC!.view.layoutIfNeeded()
            
            menuVC!.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC!.view.frame=CGRect(x: 0, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
            }, completion:nil)
            
        }else if(isOpen){
            screenCheck = false
            isOpen = false
            let viewMenuBack : UIView = view.subviews.last!
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
        }
    }
    
}

extension TimelineVC:UITableViewDelegate , UITableViewDataSource {
    
    //MARK:- TableView Delegate & DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListOfTimelineData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dict = self.arrListOfTimelineData[indexPath.row] as? [String:Any]
        
        if (dict!["image"] as! String) == "" && (dict!["video"] as! String) == "" {
            
            let cell:TimeLineTblCell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TimeLineTblCell
            
            cell.lblCellOneName.text        = (dict!["user_name"] as! String)
            cell.lblCellOneTextTitle.text   = (dict!["name"] as! String)
            cell.lblCellOneDate.text        = (dict!["created"] as! String)
            
            // profile
            cell.imgCellOneNewProfile.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            cell.imgCellOneNewProfile.sd_setImage(with: URL(string: (dict!["user_image"] as! String)), placeholderImage: UIImage(named: "logo_300"))
            
            // like
            if "\(dict!["TotalLike"]as?Int ?? 0)" == "0" {
                cell.btnCellOneLike.setTitle("\(dict!["TotalLike"]as?Int ?? 0) Like", for: .normal)
            } else if "\(dict!["TotalLike"]as?Int ?? 0)" == "1" {
                cell.btnCellOneLike.setTitle("\(dict!["TotalLike"]as?Int ?? 0) Like", for: .normal)
            } else {
                cell.btnCellOneLike.setTitle("\(dict!["TotalLike"]as?Int ?? 0) Likes", for: .normal)
            }
            
            // comments
            if "\(dict!["TotalComment"]as?Int ?? 0)" == "0" {
                cell.btnCellOneComment.setTitle("\(dict!["TotalComment"]as?Int ?? 0) Comment", for: .normal)
            } else if "\(dict!["TotalComment"]as?Int ?? 0)" == "1" {
                cell.btnCellOneComment.setTitle("\(dict!["TotalComment"]as?Int ?? 0) Comment", for: .normal)
            } else {
                cell.btnCellOneComment.setTitle("\(dict!["TotalComment"]as?Int ?? 0) Comments", for: .normal)
            }
            
            // already like
            if "\(dict!["YouLikeThePost"] as?Int ?? 0)" == "0" {
                // no already like
                cell.btnCellOneLike.setImage(UIImage(systemName: "hand.thumbsup"), for: .normal)
                
                cell.btnCellOneLike.tag = indexPath.row
                cell.btnCellOneLike.addTarget(self, action: #selector(self.tapLikeCheckHandle(_:)), for: .touchUpInside)
                
            } else {
                // yes already like
                cell.btnCellOneLike.setImage(UIImage(systemName: "hand.thumbsup.fill"), for: .normal)
            }
            
            cell.btnCellOneComment.tag = indexPath.row
            cell.btnCellOneComment.addTarget(self, action: #selector(self.tapCommentCheckHandle(_:)), for: .touchUpInside)
            
            cell.btnCellOneShare.tag = indexPath.row
            cell.btnCellOneShare.addTarget(self, action: #selector(self.tapShareCheckHandle(_:)), for: .touchUpInside)
            
        return cell
            
        } else {
            
            if (dict!["image"] as! String) == "" { // if image is empty show video
                
                let cell:TimeLineTblCell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! TimeLineTblCell
                
                cell.img_play_button.isHidden = false
                
                cell.imgCellTwoTimeLineImage.isUserInteractionEnabled = false
                
                cell.lblCellTwoName.text        = (dict!["user_name"] as! String)
                cell.lblCellTwoTextTitle.text   = (dict!["name"] as! String)
                cell.lblCellTwoDate.text        = (dict!["created"] as! String)
                
                // profile
                cell.imgCellTwoNewProfile.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                cell.imgCellTwoNewProfile.sd_setImage(with: URL(string: (dict!["user_image"] as! String)), placeholderImage: UIImage(named: "logo_300"))
                
                // timeline image
                cell.imgCellTwoTimeLineImage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                cell.imgCellTwoTimeLineImage.sd_setImage(with: URL(string: (dict!["image"] as! String)), placeholderImage: UIImage(named: "logo_300"))
                
                // like
                if "\(dict!["TotalLike"]as?Int ?? 0)" == "0" {
                    cell.btnCellTwoLike.setTitle("\(dict!["TotalLike"]as?Int ?? 0) Like", for: .normal)
                } else if "\(dict!["TotalLike"]as?Int ?? 0)" == "1" {
                    cell.btnCellTwoLike.setTitle("\(dict!["TotalLike"]as?Int ?? 0) Like", for: .normal)
                } else {
                    cell.btnCellTwoLike.setTitle("\(dict!["TotalLike"]as?Int ?? 0) Likes", for: .normal)
                }
                
                // comments
                if "\(dict!["TotalComment"]as?Int ?? 0)" == "0" {
                    cell.btnCellTwoComment.setTitle("\(dict!["TotalComment"]as?Int ?? 0) Comment", for: .normal)
                } else if "\(dict!["TotalComment"]as?Int ?? 0)" == "1" {
                    cell.btnCellTwoComment.setTitle("\(dict!["TotalComment"]as?Int ?? 0) Comment", for: .normal)
                } else {
                    cell.btnCellTwoComment.setTitle("\(dict!["TotalComment"]as?Int ?? 0) Comments", for: .normal)
                }
                
                // already like
                if "\(dict!["YouLikeThePost"] as?Int ?? 0)" == "0" {
                    // no already like
                    cell.btnCellTwoLike.setImage(UIImage(systemName: "hand.thumbsup"), for: .normal)
                    
                    cell.btnCellTwoLike.tag = indexPath.row
                    cell.btnCellTwoLike.addTarget(self, action: #selector(self.tapLikeCheckHandle(_:)), for: .touchUpInside)
                    
                } else {
                    // yes already like
                    cell.btnCellTwoLike.setImage(UIImage(systemName: "hand.thumbsup.fill"), for: .normal)
                }
                
                cell.btnCellTwoComment.tag = indexPath.row
                cell.btnCellTwoComment.addTarget(self, action: #selector(self.tapCommentCheckHandle(_:)), for: .touchUpInside)
                
                cell.btnCellTwoShare.tag = indexPath.row
                cell.btnCellTwoShare.addTarget(self, action: #selector(self.tapShareCheckHandle(_:)), for: .touchUpInside)
                
                cell.imgCellTwoTimeLineImage.tag = indexPath.row
                
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TimelineVC.video_play_when_user_hit_cell(_:)))

                cell.imgCellTwoTimeLineImage.isUserInteractionEnabled = true
                cell.imgCellTwoTimeLineImage.addGestureRecognizer(tapGestureRecognizer)
                
                return cell
                
            } else { // if video is empty
                
                let cell:TimeLineTblCell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! TimeLineTblCell
                
                cell.img_play_button.isHidden = true
                 
                cell.lblCellTwoName.text        = (dict!["user_name"] as! String)
                cell.lblCellTwoTextTitle.text   = (dict!["name"] as! String)
                cell.lblCellTwoDate.text        = (dict!["created"] as! String)
                
                // profile
                cell.imgCellTwoNewProfile.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                cell.imgCellTwoNewProfile.sd_setImage(with: URL(string: (dict!["user_image"] as! String)), placeholderImage: UIImage(named: "logo_300"))
                
                // timeline image
                cell.imgCellTwoTimeLineImage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                cell.imgCellTwoTimeLineImage.sd_setImage(with: URL(string: (dict!["image"] as! String)), placeholderImage: UIImage(named: "logo_300"))
                
                // like
                if "\(dict!["TotalLike"]as?Int ?? 0)" == "0" {
                    cell.btnCellTwoLike.setTitle("\(dict!["TotalLike"]as?Int ?? 0) Like", for: .normal)
                } else if "\(dict!["TotalLike"]as?Int ?? 0)" == "1" {
                    cell.btnCellTwoLike.setTitle("\(dict!["TotalLike"]as?Int ?? 0) Like", for: .normal)
                } else {
                    cell.btnCellTwoLike.setTitle("\(dict!["TotalLike"]as?Int ?? 0) Likes", for: .normal)
                }
                
                // comments
                if "\(dict!["TotalComment"]as?Int ?? 0)" == "0" {
                    cell.btnCellTwoComment.setTitle("\(dict!["TotalComment"]as?Int ?? 0) Comment", for: .normal)
                } else if "\(dict!["TotalComment"]as?Int ?? 0)" == "1" {
                    cell.btnCellTwoComment.setTitle("\(dict!["TotalComment"]as?Int ?? 0) Comment", for: .normal)
                } else {
                    cell.btnCellTwoComment.setTitle("\(dict!["TotalComment"]as?Int ?? 0) Comments", for: .normal)
                }
                
                // already like
                if "\(dict!["YouLikeThePost"] as?Int ?? 0)" == "0" {
                    // no already like
                    cell.btnCellTwoLike.setImage(UIImage(systemName: "hand.thumbsup"), for: .normal)
                    
                    cell.btnCellTwoLike.tag = indexPath.row
                    cell.btnCellTwoLike.addTarget(self, action: #selector(self.tapLikeCheckHandle(_:)), for: .touchUpInside)
                    
                } else {
                    // yes already like
                    cell.btnCellTwoLike.setImage(UIImage(systemName: "hand.thumbsup.fill"), for: .normal)
                }
                
                cell.btnCellTwoComment.tag = indexPath.row
                cell.btnCellTwoComment.addTarget(self, action: #selector(self.tapCommentCheckHandle(_:)), for: .touchUpInside)
                
                cell.btnCellTwoShare.tag = indexPath.row
                cell.btnCellTwoShare.addTarget(self, action: #selector(self.tapShareCheckHandle(_:)), for: .touchUpInside)
                
                return cell
                
                
                
            }
            
        }
           
    }
    
    @objc func video_play_when_user_hit_cell(_ sender:AnyObject) {
        
        let dict = self.arrListOfTimelineData[sender.view.tag] as? [String:Any]
        print(dict as Any)
        
        // let fileUrl = URL(fileURLWithPath: (dict!["video"] as! String))
        
        let url = URL(string: (dict!["video"] as! String))
        print(url as Any)
        
        // let videoURL = URL(string: self.videoURL)
        let player = AVPlayer(url:  url!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        /*let dict = self.arrListOfTimelineData[indexPath.row] as? [String:Any]
        
        if (dict!["image"] as! String) == "" {
            return UITableView.automaticDimension
        } else {
            return 0
        }*/
        
        return UITableView.automaticDimension
    }
    
}

class TimeLineTblCell: UITableViewCell {
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgContent: UIImageView!
    @IBOutlet weak var lblTime: UILabel!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblLikes: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var imgComment: UIImageView!
    @IBOutlet weak var imgShare: UIImageView!

    @IBOutlet weak var img_play_button: UIImageView!
    
    @IBOutlet weak var btnMore: UIButton!
    
    @IBOutlet weak var viewOneBG:UIView! {
        didSet {
            viewOneBG.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
            viewOneBG.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            viewOneBG.layer.shadowOpacity = 1.0
            viewOneBG.layer.shadowRadius = 15.0
            viewOneBG.layer.masksToBounds = false
            viewOneBG.layer.cornerRadius = 15
            viewOneBG.backgroundColor = .white
        }
    }
    // new
    @IBOutlet weak var imgCellOneNewProfile:UIImageView! {
        didSet {
            imgCellOneNewProfile.layer.cornerRadius = 8
            imgCellOneNewProfile.clipsToBounds = true
            
        }
    }
    
    
    
    @IBOutlet weak var lblCellOneName:UILabel! {
        didSet {
            lblCellOneName.textColor = .black
        }
    }
    
    @IBOutlet weak var lblCellOneDate:UILabel! {
        didSet {
            lblCellOneDate.textColor = .black
        }
    }
    
    @IBOutlet weak var lblCellOneTextTitle:UILabel! {
        didSet {
            lblCellOneTextTitle.textColor = .black
        }
    }
    
    @IBOutlet weak var btnCellOneLike: UIButton!
    @IBOutlet weak var btnCellOneComment: UIButton!
    @IBOutlet weak var btnCellOneShare: UIButton!
    
    // cell 2
    @IBOutlet weak var viewTwoBG:UIView! {
        didSet {
            viewTwoBG.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
            viewTwoBG.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
            viewTwoBG.layer.shadowOpacity = 1.0
            viewTwoBG.layer.shadowRadius = 15.0
            viewTwoBG.layer.masksToBounds = false
            viewTwoBG.layer.cornerRadius = 15
            viewTwoBG.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var imgCellTwoNewProfile:UIImageView! {
        didSet {
            imgCellTwoNewProfile.layer.cornerRadius = 8
            imgCellTwoNewProfile.clipsToBounds = true
            
        }
    }
    
    @IBOutlet weak var imgCellTwoTimeLineImage:UIImageView! {
        didSet {
            imgCellTwoTimeLineImage.layer.cornerRadius = 8
            imgCellTwoTimeLineImage.clipsToBounds = true
            
        }
    }
    
    @IBOutlet weak var lblCellTwoName:UILabel! {
        didSet {
            lblCellTwoName.textColor = .black
        }
    }
    
    @IBOutlet weak var lblCellTwoDate:UILabel! {
        didSet {
            lblCellTwoDate.textColor = .black
        }
    }
    
    @IBOutlet weak var lblCellTwoTextTitle:UILabel! {
        didSet {
            lblCellTwoTextTitle.textColor = .black
        }
    }
    
    @IBOutlet weak var btnCellTwoLike: UIButton!
    @IBOutlet weak var btnCellTwoComment: UIButton!
    @IBOutlet weak var btnCellTwoShare: UIButton!
    
    @IBOutlet weak var btnCellOneDot: UIButton!
    @IBOutlet weak var btnCellTwoDot: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // imgProfile.makeRounded()
        // imgProfile.layer.borderWidth = 0
    }
}

//@IBOutlet weak var btnAboutUser: UIButton!
//@IBOutlet weak var btnPostReport: UIButton!

extension TimelineVC: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
