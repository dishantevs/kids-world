//
//  HelpVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD


class UserProfileDetailsVC: UIViewController {
    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnBack: UIButton!

    @IBOutlet weak var lblEmail:UILabel!
    @IBOutlet weak var lblNumber:UILabel!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDOB:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblGender:UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    
    var responseDictionary:[String:Any] = [:]
    var dataDictionary:[String:Any] = [:]
    var status:String?
    var msg:String?

    var communityId = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.centerTextAndImage(spacing: 10.0)
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        self.getUserProfile()
    }
    

    // MARK :- button Action
    @IBAction func btnBackClickMe(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCallConnectAction(_ sender: UIButton) {
        guard let url = URL(string: "telprompt://\(lblNumber.text ?? "")"),
            UIApplication.shared.canOpenURL(url) else {
                return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    @IBAction func btnEmailTimeAction(_ sender: UIButton) {
        if let url = URL(string: "mailto:\(lblEmail.text ?? "")") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
    
    func getUserProfile() {
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let values = ["action":"profile","userId": communityId] as [String : Any]
                print("Values \(values)")
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                AF.request(request)
                    .responseJSON { response in
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            DispatchQueue.main.async {
                                print("This is run on the main queue, after the previous code in outer block")
                            }
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                self.responseDictionary = dict
                            }
                            self.status = self.responseDictionary["status"] as? String
                            if self.status == "Fails"{
                                let alertController = UIAlertController(title: "Alert", message: self.responseDictionary["msg"] as? String, preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    print("you have pressed the Ok button");
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion:nil)
                            } else {
                                let dict = self.responseDictionary["data"] as? [String: Any]
                                self.lblEmail.text = dict?["email"]as? String ?? ""
                                self.lblName.text = dict?["fullName"]as? String ?? ""
                                self.lblNumber.text = dict?["contactNumber"] as? String
                                self.lblGender.text = dict?["gender"]as? String ?? ""
                                self.lblDOB.text = dict?["dob"]as? String ?? ""
                                self.lblAddress.text = dict?["address"]as? String ?? ""
                                self.imgProfile.dowloadFromServer(link: dict?["document"]as? String ?? "", contentMode: .scaleToFill)
                            }
                        }
                    }
            }
        }
    }
}
