//
//  LeftViewController.swift
//  G.O.A.T
//
//  Created by APPLE on 28/07/20.
//  Copyright © 2020 APPLE. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

class LeftViewController: UIViewController {
    var responseDictionary:[String:Any] = [:]
    var message:String?
    var dataDictionary:[String:Any] = [:]
    var status:String?

    var objDashboard = DashboardVC()
    
    @IBOutlet var name : UILabel!
    @IBOutlet var mobile : UILabel!

    @IBOutlet var nameimaAGE : UIImageView!
    @IBOutlet var table : UITableView!
    
    var nameArray = ["Dashboard","Edit Profile","Gift","Video","Workbook","Total Points","My Friends","Friend Requests","Friend Search","Timeline","Change Password","Terms & Condition","Help","Sign out"]
    
    var imgArray = ["home","edit","gifts","play","wordbook","wordbook","comment1","friends","friends","timeline","lock","note","helpL","logout"]
    
    var dict : [String : Any] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameimaAGE.layer.cornerRadius = self.nameimaAGE.frame.width / 2
        self.nameimaAGE.layer.masksToBounds = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.dict = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]

        // print(dict as Any)
        
        if(UserDefaults.standard.string(forKey: "profileUpdate") == "1") {
            DispatchQueue.main.async {
                self.name.text = (self.dict["fullName"]as? String ?? "").uppercased()
                self.mobile.text = self.dict["elementarySchool"]as? String ?? ""
                if (self.dict["image"]as? String ?? "" != "") {
                    self.nameimaAGE.dowloadFromServer(link: self.dict["image"] as? String ?? "", contentMode: .scaleToFill)
                }else{
                    self.nameimaAGE.image = UIImage(named: "user-1")
                }
            }
        } else {
            DispatchQueue.main.async {
                self.name.text = UserDefaults.standard.string(forKey:"fullName")?.uppercased()
                self.mobile.text = self.dict["elementarySchool"]as? String ?? ""

                if (UserDefaults.standard.string(forKey:"image") != "") {
                    self.nameimaAGE.dowloadFromServer(link:UserDefaults.standard.string(forKey:"image") ?? "", contentMode: .scaleToFill)
                }else{
                    self.nameimaAGE.image = UIImage(named: "user-1")
                }
            }
        }
    }
}

extension LeftViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellReuseIdentifier = "LeftCell"
        let cell:LeftCell = self.table.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! LeftCell
        cell.nameLbL.text = nameArray[indexPath.row]
        cell.img.image = UIImage(named: imgArray[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as? DashboardVC
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 1{ // Edit Profile
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 2{ // Gift
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "GiftsListingVC") as? GiftsListingVC
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 3{ // Video
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "VideosListingVC") as? VideosListingVC
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 4{ // Workbook
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "WorkbookListingVC") as? WorkbookListingVC
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 5{ // points
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "TotalPointsVC") as? TotalPointsVC
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 6{ // Chats // Friends
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "ChatFriendsListingVC") as? ChatFriendsListingVC
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 7{ // Friends Listing
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "FriendsListingVC") as? FriendsListingVC
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 8{ // Send F Request
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "SendFriendsListingVC") as? SendFriendsListingVC
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 9{ // Time Line
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "TimelineVC") as? TimelineVC 
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 10{ //
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 11{ // Terms Condition
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "TermsConditionVC") as? TermsConditionVC
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 12{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "HelpVC") as? HelpVC
            self.navigationController?.pushViewController(menuVC!, animated: false)
        }else if indexPath.row == 13{
           // NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)

            let alertController = UIAlertController(title: "Alert", message: "Are you sure you want to signout?", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            }
            let okAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                self.logoutAPI()
            }
            alertController.addAction(cancel)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion:nil)
        }
    }
    
    func logoutAPI(){
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
            
            if let apiString = URL(string:BaseUrl) {
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let values = ["action":"logout","userId":String(dict["userId"]as? Int ?? 0)] as [String : Any]
                print("Values \(values)")
                
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                AF.request(request)
                    .responseJSON { response in
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            MBProgressHUD.hide(for:self.view, animated: true)
                            self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            DispatchQueue.main.async {
                                print("This is run on the main queue, after the previous code in outer block")
                            }
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                self.responseDictionary = dict
                            }
                            
                            self.status = self.responseDictionary["status"] as? String
                            self.message = self.responseDictionary["msg"] as? String
                            let alertController = UIAlertController(title: "Logout?", message:self.status, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                if self.status != "Fails" {
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = storyboard.instantiateViewController(withIdentifier: "WelcomeVC") as?  WelcomeVC
                                        let aObjNavi = UINavigationController(rootViewController: vc!)
                                        aObjNavi.isNavigationBarHidden = true

                                        UserDefaults.standard.removeObject(forKey: "login_status")
                                        UserDefaults.standard.removeObject(forKey:"loginStatus")
                                        UserDefaults.standard.removeObject(forKey: "kAPI_LOGIN_DATA")

                                        if let scene = UIApplication.shared.connectedScenes.first{
                                        guard let windowScene = (scene as? UIWindowScene) else { return }
                                        print(">>> windowScene: \(windowScene)")
                                        let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                                        window.windowScene = windowScene
                                        window.rootViewController = aObjNavi
                                        window.makeKeyAndVisible()
                                        appDelegate.window = window
                                    }
                                }
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
            }
        }
    }
}

class LeftCell: UITableViewCell {
    @IBOutlet var nameLbL : UILabel!
    @IBOutlet var img : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
