//
//  ViewController.swift
//  AVPlayerExample
//
//  Created by Satyadev on 22/01/18.
//  Copyright © 2018 Satyadev Chauhan. All rights reserved.
//

import UIKit
import AVKit

class VideoPlayerVC: UIViewController {
    
    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var lblNavTitile: UILabel!

    var strUrl = ""
    var strTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.centerTextAndImage(spacing: 10.0)
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)

        self.btnBack.addTarget(self, action: #selector(sideBarBackClick), for: .touchUpInside)
        self.lblNavTitile.text = strTitle.uppercased()

        guard let url = URL(string: strUrl) else {
            return
        }
        self.playVideo(videoUrl: url, to: self.playerView)
    }
    
    func playVideo(videoUrl: URL, to view: UIView) {
        let player = AVPlayer(url: videoUrl)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.addChild(playerController)
        playerController.view.frame = view.bounds
        view.addSubview(playerController.view)
        player.play()
    }
    
    
    @objc func sideBarBackClick() {
        self.navigationController?.popViewController(animated: true)
    }
}

