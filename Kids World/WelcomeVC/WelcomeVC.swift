//
//  WelcomeVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import QuartzCore

class WelcomeVC: UIViewController {

    @IBOutlet weak var viewFullNavConstraint : NSLayoutConstraint!
    // self.viewFullNavConstraint.constant = 64 //
    
    @IBOutlet weak var viewNavBG:UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)

        
        if(UserDefaults.standard.string(forKey: "loginStatus") == "1") {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DashboardVC") as? DashboardVC
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
    // MARK :- button Action
    
    @IBAction func btnSignInClickMe(_ sender : UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    @IBAction func btnCreateAccountClickMe(_ sender : UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegisterVC") as? RegisterVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
}
