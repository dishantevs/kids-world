//
//  WorkbookListingVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import MBProgressHUD
import SwiftyJSON
import AVFoundation


class ChapterResultVC: UIViewController {

    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblRight: UILabel!
    @IBOutlet weak var lblWorng: UILabel!
    @IBOutlet weak var viewNavBG:UIView!
    
    @IBOutlet weak var viewRight:UIView!
    @IBOutlet weak var viewWrong:UIView!

    @IBOutlet weak var btnBack: UIButton!
//    // slider//
//    var isOpen = false
//    var screenCheck = false

    var strTotal = Int()
    var strRight = Int()
//    var intWrong = Int()

    var strWrong = ""
    
    let winSound = URL(fileURLWithPath: Bundle.main.path(forResource: "win", ofType: "mp3")!)
    var audioPlayer = AVAudioPlayer()

    var timer : Timer?
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.centerTextAndImage(spacing: 10.0)
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
        viewWrong.layer.cornerRadius = 5
        viewRight.layer.cornerRadius = 5
        
        self.lblTotal.text = "Total Questions: \(strTotal)"
        self.lblRight.text = "Right Answer - \(strRight)"
        self.lblWorng.text = "Wrong Answer - \(strTotal - strRight)"
//        intWrong = strTotal - strRight
        
        
        if strTotal == strRight {
            
            do {
                
                audioPlayer =  try AVAudioPlayer(contentsOf: winSound)
                audioPlayer.play()
                timer = Timer.scheduledTimer(timeInterval:1, target:self, selector:#selector(prozessTimer), userInfo: nil, repeats: true)
                
            } catch {
            }
            
            
            
            
            
        } else {
            // some answers are wrong
            print("some answers are wrong")
            
            let alertController = UIAlertController(title: "Alert", message: "Better luck next time", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
            }
            
            alertController.addAction(cancel)
            
            self.present(alertController, animated: true, completion:nil)
            
        }
        
    }

    @objc func prozessTimer() {
        counter += 1
        print("This is a second ", counter)

        if counter == 3 {
            timer?.invalidate()
            
            audioPlayer.stop()
        }
        
    }
    
    // MARK :- button Action
    @IBAction func btnBackClickMe(_ sender : UIButton) {
        audioPlayer.stop()
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WorkbookListingVC") as? WorkbookListingVC
        self.navigationController?.pushViewController(vc!, animated: false)
    }
}
