//
//  WorkbookListingVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import MBProgressHUD
import SwiftyJSON
import AVFoundation

class ChapterTestVC: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var lblNavTitile: UILabel!
    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    //sam
    var currentPageNumber:Int = 1
    var totalPage: Int = 0
    var arrJSONFull  = [AnyObject]()
    var arrJSON:[[String:Any]] = [[:]]
    
    var arrListOfAllMyOrders:NSMutableArray! = []
    var responseDictionary:[String:Any] = [:]
    var fetchNewArray: [[String:Any]] = [[:]]
    var DictGetValueIndex:[String:Any] = [:]
    
    
    var arrQuestionAnswerListing:[[String:Any]] = [[:]]
    var arrQuestionAnswerShow : NSMutableArray = NSMutableArray()
    
    
    var indexOfTblTag = Int()
    var tblIndex = NSIndexPath()
    
    let clickSound = URL(fileURLWithPath: Bundle.main.path(forResource: "quiz_option_click", ofType: "mp3")!)
    var audioPlayer = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbleView.isHidden = true
//        lblNoRecord.isHidden = false
        btnBack.centerTextAndImage(spacing: 10.0)
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
        self.lblNavTitile.text = (DictGetValueIndex["name"]as? String ?? "").uppercased()
        self.arrJSON.removeAll()
        self.getGoalResponseData(pageNo : currentPageNumber)
        
        self.btnSubmit.isHidden = true
    }
    
    // MARK :- button Action
    @IBAction func btnBackClickMe(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WorkbookChapterVC") as? WorkbookChapterVC
//        self.navigationController?.pushViewController(vc!, animated: false)
    }
    
    func getGoalResponseData(pageNo: Int){
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let values = ["action":"quizlist","workbookId":self.DictGetValueIndex["workbookId"]as?Int ?? 0,"exerciseId":self.DictGetValueIndex["exerciseId"]as?Int ?? 0]as [String : Any]as [String : Any]
                print("Values \(values)")
                
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                AF.request(request)
                    .responseJSON { response in
                        // do whatever you want here
                        MBProgressHUD.hide(for:self.view, animated: true)
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            DispatchQueue.main.async {
                                print("This is run on the main queue, after the previous code in outer block")
                            }
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            //     [anshwerlist] => [{"questionId":"10","answer":"option_1"},{"questionId":"9","answer":""}]
                            if let dict = responseObject as? [String:Any] {
                                self.arrJSONFull  = (dict["data"] as AnyObject) as! [AnyObject]
                                if (self.arrJSONFull.count != 0){
                                    self.tbleView.isHidden = false
                                    self.tbleView.reloadData()
                                    for index in 0..<self.arrJSONFull.count {
                                        let element = self.arrJSONFull[index]
                                        let paraInfo:NSMutableDictionary = NSMutableDictionary()
                                        paraInfo.setValue( element["questionId"]as? Int ?? 0, forKey: "questionId")
                                        paraInfo.setValue( "", forKey: "answer")
                                        self.arrQuestionAnswerShow.insert(paraInfo, at: index)
                                    }
                                    print("arr Question Answer Show :: \(self.arrQuestionAnswerShow)")
                                }else{
                                    self.tbleView.isHidden = true
                                }
                            }
                        }
                    }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrJSONFull.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ChapterCell = tableView.dequeueReusableCell(withIdentifier: "ChapterCell") as! ChapterCell
        
        let dict : [String :Any] = arrJSONFull[indexPath.row] as! [String : Any]
        cell.lblQuestion.text = (dict["question"]as? String ?? "")
        
        let array = dict["option"] as? [[String : Any]] ?? [[:]]
        let dictOption1 : [String :Any] = array[0] as? [String : Any] ?? [:]
        let dictOption2 : [String :Any] = array[1] as? [String : Any] ?? [:]
        let dictOption3 : [String :Any] = array[2] as? [String : Any] ?? [:]
        let dictOption4 : [String :Any] = array[3] as? [String : Any] ?? [:]
        
        cell.lblAnswer1.text = "\(dictOption1["value"]as? String ?? "")"
        cell.lblAnswer2.text = "\(dictOption2["value"]as? String ?? "")"
        cell.lblAnswer3.text = "\(dictOption3["value"]as? String ?? "")"
        cell.lblAnswer4.text = "\(dictOption4["value"]as? String ?? "")"
        
        cell.lblAnswer1Key.text = "\(dictOption1["key"]as? String ?? "")"
        cell.lblAnswer2Key.text = "\(dictOption2["key"]as? String ?? "")"
        cell.lblAnswer3Key.text = "\(dictOption3["key"]as? String ?? "")"
        cell.lblAnswer4Key.text = "\(dictOption4["key"]as? String ?? "")"
        
        
        cell.btn1.tag = indexPath.row
        cell.btn2.tag = indexPath.row
        cell.btn3.tag = indexPath.row
        cell.btn4.tag = indexPath.row
        
        cell.btn1.addTarget(self, action: #selector(self.btn1ClickMe(_:)), for: .touchUpInside)
        cell.btn2.addTarget(self, action: #selector(self.btn2ClickMe(_:)), for: .touchUpInside)
        cell.btn3.addTarget(self, action: #selector(self.btn3ClickMe(_:)), for: .touchUpInside)
        cell.btn4.addTarget(self, action: #selector(self.btn4ClickMe(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func btn1ClickMe(_ sender : UIButton) { // indexPath?.row = 0 //
        indexOfTblTag = sender.tag
        self.btnSubmit.isHidden = false
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tbleView)
        let indexPath = self.tbleView.indexPathForRow(at: buttonPosition)
        let cell:ChapterCell = (self.tbleView.cellForRow(at: indexPath!) as? ChapterCell)!
        
        cell.img1.image = #imageLiteral(resourceName: "checked")
        cell.img2.image = #imageLiteral(resourceName: "uncheck")
        cell.img3.image = #imageLiteral(resourceName: "uncheck")
        cell.img4.image = #imageLiteral(resourceName: "uncheck")
        
        let dict : [String :Any] = arrJSONFull[indexPath!.row] as! [String : Any]
        for index in 0..<self.arrQuestionAnswerShow.count {
            let element = self.arrQuestionAnswerShow[index] as? [String:Any]
            if((dict["questionId"]as? Int ?? 0) == element?["questionId"]as? Int ?? 0){
                self.arrQuestionAnswerShow.removeObject(at: indexPath?.row ?? 0)
                let paraInfo:NSMutableDictionary = NSMutableDictionary()
                paraInfo.setValue( element?["questionId"]as? Int, forKey: "questionId")
                paraInfo.setValue( cell.lblAnswer1Key.text ?? "", forKey: "answer")
                self.arrQuestionAnswerShow.insert(paraInfo, at: indexPath?.row ?? 0)
            }
        }
        
        do {
             audioPlayer = try AVAudioPlayer(contentsOf: clickSound)
             audioPlayer.play()
        } catch {
        }
    }
    
    @objc func btn2ClickMe(_ sender : UIButton) {
        indexOfTblTag = sender.tag
        self.btnSubmit.isHidden = false
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tbleView)
        let indexPath = self.tbleView.indexPathForRow(at: buttonPosition)
        let cell:ChapterCell = (self.tbleView.cellForRow(at: indexPath!) as? ChapterCell)!
        
        cell.img1.image = #imageLiteral(resourceName: "uncheck")
        cell.img2.image = #imageLiteral(resourceName: "checked")
        cell.img3.image = #imageLiteral(resourceName: "uncheck")
        cell.img4.image = #imageLiteral(resourceName: "uncheck")
        
        let dict : [String :Any] = arrJSONFull[indexPath!.row] as! [String : Any]
        for index in 0..<self.arrQuestionAnswerShow.count {
            let element = self.arrQuestionAnswerShow[index] as? [String:Any]
            if((dict["questionId"]as? Int ?? 0) == element?["questionId"]as? Int ?? 0){
                self.arrQuestionAnswerShow.removeObject(at: indexPath?.row ?? 0)
                let paraInfo:NSMutableDictionary = NSMutableDictionary()
                paraInfo.setValue( element?["questionId"]as? Int, forKey: "questionId")
                paraInfo.setValue( cell.lblAnswer2Key.text ?? "", forKey: "answer")
                self.arrQuestionAnswerShow.insert(paraInfo, at: indexPath?.row ?? 0)
            }
        }
        do {
             audioPlayer = try AVAudioPlayer(contentsOf: clickSound)
             audioPlayer.play()
        } catch {
        }
    }
    
    @objc func btn3ClickMe(_ sender : UIButton) {
        indexOfTblTag = sender.tag
        self.btnSubmit.isHidden = false
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tbleView)
        let indexPath = self.tbleView.indexPathForRow(at: buttonPosition)
        let cell:ChapterCell = (self.tbleView.cellForRow(at: indexPath!) as? ChapterCell)!
        
        cell.img1.image = #imageLiteral(resourceName: "uncheck")
        cell.img2.image = #imageLiteral(resourceName: "uncheck")
        cell.img3.image = #imageLiteral(resourceName: "checked")
        cell.img4.image = #imageLiteral(resourceName: "uncheck")
        
        let dict : [String :Any] = arrJSONFull[indexPath!.row] as! [String : Any]
        for index in 0..<self.arrQuestionAnswerShow.count {
            let element = self.arrQuestionAnswerShow[index] as? [String:Any]
            if((dict["questionId"]as? Int ?? 0) == element?["questionId"]as? Int ?? 0){
                self.arrQuestionAnswerShow.removeObject(at: indexPath?.row ?? 0)
                let paraInfo:NSMutableDictionary = NSMutableDictionary()
                paraInfo.setValue( element?["questionId"]as? Int, forKey: "questionId")
                paraInfo.setValue( cell.lblAnswer3Key.text ?? "", forKey: "answer")
                self.arrQuestionAnswerShow.insert(paraInfo, at: indexPath?.row ?? 0)
            }
        }
        do {
             audioPlayer = try AVAudioPlayer(contentsOf: clickSound)
             audioPlayer.play()
        } catch {
        }
    }
    
    @objc func btn4ClickMe(_ sender : UIButton) {
        indexOfTblTag = sender.tag
        self.btnSubmit.isHidden = false
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tbleView)
        let indexPath = self.tbleView.indexPathForRow(at: buttonPosition)
        let cell:ChapterCell = (self.tbleView.cellForRow(at: indexPath!) as? ChapterCell)!
        
        cell.img1.image = #imageLiteral(resourceName: "uncheck")
        cell.img2.image = #imageLiteral(resourceName: "uncheck")
        cell.img3.image = #imageLiteral(resourceName: "uncheck")
        cell.img4.image = #imageLiteral(resourceName: "checked")
        
        let dict : [String :Any] = arrJSONFull[indexPath!.row] as! [String : Any]
        for index in 0..<self.arrQuestionAnswerShow.count {
            let element = self.arrQuestionAnswerShow[index] as? [String:Any]
            if((dict["questionId"]as? Int ?? 0) == element?["questionId"]as? Int ?? 0){
                self.arrQuestionAnswerShow.removeObject(at: indexPath?.row ?? 0)
                let paraInfo:NSMutableDictionary = NSMutableDictionary()
                paraInfo.setValue( element?["questionId"]as? Int, forKey: "questionId")
                paraInfo.setValue( cell.lblAnswer4Key.text ?? "", forKey: "answer")
                self.arrQuestionAnswerShow.insert(paraInfo, at: indexPath?.row ?? 0)
            }
        }
        do {
             audioPlayer = try AVAudioPlayer(contentsOf: clickSound)
             audioPlayer.play()
        } catch {
        }
    }
    
    @IBAction func btnSubmitClickMe(_ sender : UIButton) {
        self.callQuizSubmitionAPI()
    }
    
    
    func callQuizSubmitionAPI(){
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                let paramsArray = self.arrQuestionAnswerShow
                let paramsJSON = JSON(paramsArray)
                let paramsString = paramsJSON.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!
                
                let values = ["action":"addanswer","userId":String(dict["userId"]as? Int ?? 0),"type":"2","anshwerlist":paramsString]as [String : Any]as [String : Any]
                print("Values \(values)")
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                AF.request(request)
                    .responseJSON { response in
                        // do whatever you want here
                        MBProgressHUD.hide(for:self.view, animated: true)
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            // self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            if let dict = responseObject as? [String:Any] {
                                    let alertController = UIAlertController(title: "Alert", message: dict["msg"]as? String ?? "", preferredStyle: .alert)
                                    let okAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                        if(dict["status"]as? String ?? "" != "Fails"){
                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                            let menuVC  = storyboard.instantiateViewController(withIdentifier: "ChapterResultVC") as? ChapterResultVC
                                            menuVC?.strTotal = dict["Total"]as? Int ?? 0
                                            menuVC?.strRight = dict["correct"]as? Int ?? 0
                                            self.navigationController?.pushViewController(menuVC!, animated: true)
                                        }
                                    }
                                    alertController.addAction(okAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                        }
                    }
                }
            }
}



class ChapterCell: UITableViewCell {
    @IBOutlet weak var lblQuestion: UILabel!
    
    @IBOutlet weak var lblAnswer1: UILabel!
    @IBOutlet weak var lblAnswer2: UILabel!
    @IBOutlet weak var lblAnswer3: UILabel!
    @IBOutlet weak var lblAnswer4: UILabel!
    
    @IBOutlet weak var lblAnswer1Key: UILabel!
    @IBOutlet weak var lblAnswer2Key: UILabel!
    @IBOutlet weak var lblAnswer3Key: UILabel!
    @IBOutlet weak var lblAnswer4Key: UILabel!
    
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
}
