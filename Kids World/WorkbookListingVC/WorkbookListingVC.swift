//
//  WorkbookListingVC.swift
//  Kids World
//
//  Created by apple on 22/04/21.
//

import UIKit
import Alamofire
import MBProgressHUD
import SwiftyJSON


class WorkbookListingVC: UIViewController , UITableViewDelegate , UITableViewDataSource {

    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var viewNavBG:UIView!
    @IBOutlet weak var btnMenu: UIButton!
    // slider//
    var isOpen = false
    var screenCheck = false

    //sam
    var currentPageNumber:Int = 1
    var totalPage: Int = 0
    var arrJSON  = [AnyObject]()
    
    var arrListOfAllMyOrders:NSMutableArray! = []
    var responseDictionary:[String:Any] = [:]
    var fetchNewArray: [[String:Any]] = [[:]]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbleView.isHidden = true
        lblNoRecord.isHidden = false
        btnMenu.centerTextAndImage(spacing: 10.0)        
        self.viewNavBG.applyGradient(colors: [UIColor.init(red: 0/255, green: 115/255, blue: 188/255, alpha: 1).cgColor, UIColor.init(red: 149/255, green: 210/255, blue: 107/255, alpha: 1).cgColor], locations: nil, direction: .leftToRight)
        
        // slider //
        self.btnMenu.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        let directions: [UISwipeGestureRecognizer.Direction] = [.left,.right,.up,.down]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(DashboardVC.handleSwipe1(gesture:)))
            gesture.direction = direction
            self.view?.addGestureRecognizer(gesture)
        }

        self.arrJSON.removeAll()
        self.getGoalResponseData(pageNo : currentPageNumber)
    }

    // MARK :- button Action
    @IBAction func btnBackClickMe(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func getGoalResponseData(pageNo: Int){
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Alert", message: "Device is not connected to Internet", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
//            let dict : [String : Any] = UserDefaults.standard.dictionary(forKey: "kAPI_LOGIN_DATA") ?? [:]
            let spinnerActivity = MBProgressHUD.showAdded(to:view, animated: true);
            spinnerActivity.label.text = "Loading";
            spinnerActivity.detailsLabel.text = "Please Wait!!";
            
            if let apiString = URL(string:BaseUrl) {
                var request = URLRequest(url:apiString)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let values = ["action":"workbooklist","pageNo":pageNo]as [String : Any]as [String : Any]
                print("Values \(values)")
                request.httpBody = try! JSONSerialization.data(withJSONObject: values)
                AF.request(request)
                    .responseJSON { response in
                        // do whatever you want here
                        MBProgressHUD.hide(for:self.view, animated: true)
                        switch response.result {
                        case .failure(let error):
                            let alertController = UIAlertController(title: "Alert", message: "Some error Occured", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                print("you have pressed the Ok button");
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion:nil)
                            print(error)
                            if let data = response.data,
                               let responseString = String(data: data, encoding: .utf8) {
                                print(responseString)
                                print("response \(responseString)")
                            }
                        case .success(let responseObject):
                            DispatchQueue.main.async {
                                print("This is run on the main queue, after the previous code in outer block")
                            }
                            print("response \(responseObject)")
                            MBProgressHUD.hide(for:self.view, animated: true)
                            
                            if let dict = responseObject as? [String:Any] {
                                let status = dict["status"]as? String
                                if(status != "Fails"){
                                    self.fetchNewArray.removeAll()
                                    if (self.currentPageNumber == 1) {
                                        self.arrJSON  = (dict["data"] as AnyObject) as! [AnyObject]
                                    } else {
                                        self.fetchNewArray  = ((dict["data"] as AnyObject) as! [AnyObject] as NSArray) as! [[String : Any]]
                                    }
                                    if self.arrJSON.count != 0{
                                        if (self.currentPageNumber == 1){
                                            self.tbleView.isHidden = false
                                            self.lblNoRecord.isHidden = true
                                            self.tbleView.reloadData()
                                        }else{
                                            if (self.fetchNewArray.count != 0) {
                                                self.didFinishRecordsRequest(result: (self.fetchNewArray) as NSArray, pageNo: self.currentPageNumber)
                                            }else {
                                                self.currentPageNumber -= 1
                                            }
                                        }
                                    }else{
                                        self.tbleView.isHidden = true
                                    }
                                }else{
                                    self.tbleView.isHidden = true
                                }
                            }else{
                                self.tbleView.isHidden = true
                            }
                        }
                    }
                }
            }
        }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if(currentPageNumber != 1){
            let lastItem = self.arrJSON.count - 1
            if indexPath.row == lastItem {
                print("IndexRow :: \(indexPath.row)")
                self.currentPageNumber += 1
                self.getGoalResponseData(pageNo: self.currentPageNumber)
            }
        }
    }
    func didFinishRecordsRequest(result:NSArray, pageNo:Int) {
        if (pageNo != 0){
            self.arrJSON += result as Array<AnyObject>
        } else {
            self.arrJSON.removeAll()
            self.arrJSON = result.mutableCopy() as! [AnyObject]
        }
        self.totalPage = Int(self.arrJSON.count)
        self.tbleView.reloadData()
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrJSON.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:WorkLookCell = tableView.dequeueReusableCell(withIdentifier: "WorkLookCell") as! WorkLookCell
        let dict : [String :Any] = arrJSON[indexPath.row] as! [String : Any]
        cell.lblTitile.text = dict["name"]as? String ?? ""
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict : [String :Any] = arrJSON[indexPath.row] as! [String : Any]
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WorkbookChapterVC") as? WorkbookChapterVC
        vc?.DictGetValue = dict
        self.navigationController?.pushViewController(vc!, animated: false)
    }
    // Left Slider //
    @objc func handleSwipe1(gesture: UISwipeGestureRecognizer) {
        print(gesture.direction)
        switch gesture.direction {

        case UISwipeGestureRecognizer.Direction.left:
            isOpen = false
            if screenCheck == true{
                let viewMenuBack : UIView = view.subviews.last!
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    var frameMenu : CGRect = viewMenuBack.frame
                    frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                    viewMenuBack.frame = frameMenu
                    viewMenuBack.layoutIfNeeded()
                    viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
                    
                })
                screenCheck = false
            }else{
            }
            print("left swipe")
        case UISwipeGestureRecognizer.Direction.right:
            print("right swipe")
            isOpen = true
            if screenCheck == false{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let menuVC  = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController
                self.view.addSubview(menuVC!.view)
                self.addChild(menuVC!)
                menuVC!.view.layoutIfNeeded()
                
                menuVC!.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
                
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    menuVC!.view.frame=CGRect(x: 0, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
                }, completion:nil)
                screenCheck = true
            }else{
        }
        default:
            print("other swipe")
        }
    }
    @objc func sideBarMenuClick() {
        if(!isOpen){
            screenCheck = true
            isOpen = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let menuVC  = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as? LeftViewController
            self.view.addSubview(menuVC!.view)
            self.addChild(menuVC!)
            menuVC!.view.layoutIfNeeded()
            
            menuVC!.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC!.view.frame=CGRect(x: 0, y: 88, width: UIScreen.main.bounds.size.width-100, height: UIScreen.main.bounds.size.height - 88);
            }, completion:nil)
            
        }else if(isOpen){
            screenCheck = false
            isOpen = false
            let viewMenuBack : UIView = view.subviews.last!
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
        }
    }
}


class WorkLookCell: UITableViewCell {
    @IBOutlet weak var lblTitile: UILabel!
}
